<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notification;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Gate::allows('requestor')){
            return view('pages.createTicket');
        }
        return view('home');
    }

    public function readNotification(){
        Notification::where('user_id', \Auth::user()->id)->update([
            'isRead'=>1
        ]);
    }
}
