@extends('adminlte::page')

@section('title', 'E-Mail Notifications | IT Service Desk')

@section('content_header')
    <h1>E-Mail Notifications</h1>
@stop

@section('content')
	<div class='notifications top-right'></div>
	<table id="emailTable" class="table table-striped table-hover table-bordered text-center">
		<thead>
			<tr>
				<th>Subject</th>
				<th>Request Status</th>
				<th>Body</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
	</table>
@stop

@section('js')
	<script type="text/javascript">
		@isset ($emailUpdated)
		    $('.top-right').notify({
		    	message: { text: "E-mail template updated successfully." }
		    }).show();
		@endisset
		var table = $('#emailTable').DataTable({
			processing: true,
			'order': [],
			ajax: '{{ route('setup-email-notif') }}',
			columns: [
				{ data: 'subject', width: '20%' },
				{ data: 'request_status', width: '20%' },
				{ data: 'body', width: '20%' },
				{ data: 'status', width: '100px', sortable: false },
				{ data: null, width: '100px', searchable: false, sortable: false }
			],
			dom: 'r<"pull-right"B><"pull-left"lf >tip',
			lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
			pageLength: 10,
			columnDefs: [
			{
				targets: -1,
				render: function(a, b, data, d){
					var btn = '';
					btn += "<a href='/setup/email-notifications/"+ data.id +"/edit' data-id='"+ data.id +"' class='btn btn-default btn-sm reopen'>Update</a>";
					if(data.status == 1){
						btn += "<a href='#' data-id='"+ data.id +"' data-status='0' class='btn btn-default btn-sm updateBtn'>Deactivate</a>";
					}else{
						btn += "<a href='#' data-id='"+ data.id +"' data-status='1' class='btn btn-default btn-sm updateBtn'>Activate</a>";
					}
					return btn;
				}
			},
			{
				targets: [1],
				render: function(data, type, row){
					if(data == null){
						return 'Pending';
					}
					var color = "";
					switch(row.status_id){
						case '1':
							color = 'gray';
							break;
						case '2':
							color = 'primary';
							break;
						case '4': 
							color = 'red';
							break;
						case '5':
							color = 'green';
							break;
						case '6':
							color = 'gray';
							break;
						case '7':
							color = 'green';
							break;
						case '8':
							color = 'danger';
							break;
						case '9':
							color = 'green';
							break;
						case '10':
							color = 'maroon';
							break;
						case '11':
						case '12':
							color = 'orange';
							break;
						case '13':
							color = 'navy';
							break;
						default:
							color = 'black';
							break;
					}


					return '<span class="label bg-'+ color +'">'+data+'</span>';
				}
			},
			{
				targets: [2],
				render: function(data, type, row){
					return data.length > 20 ? '<span data-toggle="tooltip" title="' + data + '">' + data.substr( 0, 20 ) + '...' + '</span>':
						data;
				}
			},
			{
				targets: [3],
				render: function(data, type, row){
					return data == 1 ? '<span class="label bg-green">Active</span>' : '<span class="label bg-red">Inactive</span>';
				}
			},
			],
			buttons: [
			{
				extend: 'excel',
				text: 'Export',
				exportOptions: {
					columns: [0,1,2,3]
				}
			},
			],
			"scrollX": true,
			"fixedHeader": true
		});

		$('#emailTable').on('draw.dt', function () {
	        $('[data-toggle="tooltip"]').tooltip();
	    });

	    $(document).on('click', '.updateBtn', function(e){
	    	e.preventDefault();
	    	var id = $(this).data('id');
	    	var status_id = $(this).data('status');
	    	var type = '';
	    	switch(status_id){
	    		case 1:
	    			type = "Activate";
	    			break;
	    		case 0:
	    			type = "Deactivate";
	    			break;
	    		default:
	    			swal('Error', 'Invalid Request, reloading page.', 'error').then(function(){
	    				window.location.reload();
	    			});
	    			break;
	    	}
	    	swal({
	    		text: 'Are you sure you want to '+ type +' this template?',
	    		showCancelButton: true,
	    		cancelButtonText: 'No',
	    		confirmButtonText: 'Yes',
	    		type: 'question',
	    		showLoaderOnConfirm: true
	    	}).then(function(){
	    		$.ajax({
	    			url: '../setup/email-notifications/' + id,
	    			type: 'PATCH',
	    			data: {activate: true, status: status_id},
	    			dataType: 'JSON',
	    			success: function(data, result){
	    				swal(data.title, data.msg, data.type).then(function(){
	    					table.ajax.reload();
	    				});
	    			}
	    		});
	    	});
	    })
	</script>
@stop