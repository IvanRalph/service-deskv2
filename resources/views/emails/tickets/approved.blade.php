@component('mail::message')
# [APPROVED] IT Support Ticket No. {{ $content['ticket_no'] }}

Hi, {{ $content['requestor'] }}.

Your request with TIcket No. {{ $content['ticket_no'] }} has been approved. An IT representative will assist you shortly.

You may view ticket's progress online by clicking the button below.


@component('mail::button', ['url' => config('app.url')])
Click Here
@endcomponent

Thanks,<br>
{{ config('app.name') }}

<hr>
<span style="font-size: 10px;">If you’re having trouble clicking the button above, you may copy and paste the URL below into your web browser: {{ config('app.url') }}
</span>
@endcomponent
