<?php

use Illuminate\Database\Seeder;

class ResolutionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resolution_codes')->insert([
        	'code'=>'RES-2018',
        	'description'=>'test resolution'
        ]);
    }
}
