@extends('adminlte::page')

@section('title', 'Tickets | IT Service Desk')

@section('content_header')

@stop

@section('content')
<div class='notifications top-right'></div>
	<div class="row">
		<div class="col-sm-2">
			<a href="@if(\Gate::allows('it-staff')) {{ action('ItSupportTicketController@index') }} @elseif(isset($approver)) {{ action('ApprovalController@index') }} @elseif(isset($showTicket)) {{ action('TicketController@index') }} @else {{ action('TicketController@index') }} @endif" style="color: #333; font-size: 35px!important;"><i class="fa fa-arrow-left"></i></a>
		</div>
	</div>
	<div class="row">
		<form action="@if(isset($updateTicket)) {{ action('TicketController@update', $updateTicket->id) }} @elseif(\Gate::allows('it-staff') && isset($showTicket)) {{ action('ItSupportTicketController@update', $showTicket->id) }} @elseif(!isset($showTicket) && !isset($updateTicket)) {{ action('TicketController@store') }} @endif" method="POST">
			@csrf
			@isset ($updateTicket)
			@method('PATCH')
			@endisset
			@if (\Gate::allows('it-staff') && isset($showTicket))
				@method('PATCH')
			@endif
			<div class="col-md-6 col-md-offset-3">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-ticket"></i> @if(isset($showTicket)) View @elseif(isset($updateTicket)) Edit @else Open @endif Ticket {{ isset($showTicket) ? 'NO. ' . $showTicket->ticket_no : '' }}{{ isset($updateTicket) ? 'NO. ' . $updateTicket->ticket_no : '' }}</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="{{ (!isset($updateTicket) && !isset($showTicket)) || \Gate::denies('it-staff') ? 'hidden' : '' }}">
							<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
								<label for="type">Type</label>
								@if (isset($showTicket) && \Gate::denies('it-staff'))
								<input type="text" class="form-control" name="" readonly value="{{ isset($showTicket->requestType) ? $showTicket->requestType->typeChild->first()->title : 'To be filled up by IT Staff' }}">
								@else
								<select class="form-control" name="type" {{ isset($updateTicket) || (isset($showTicket) && $showTicket->assigned_to != \Auth::user()->id) || isset($itSupportApproval) ? 'disabled' : '' }}>
									<option value="">Select Type</option>
									@foreach (App\RequestType::get() as $type)
										@foreach ($type->typeChild as $t)
										<option value="{{ $t->id }}">{{ $t->title }}</option>
										@endforeach
									@endforeach
								</select>
								@endif
								@if($errors->has('type'))
								<span id="helpBlock2" class="help-block">{{ $errors->first('type') }}</span>
								@endif
							</div>

							<div class="form-group {{ $errors->has('category') ? 'has-error' : '' }}">
								<label for="category">Category</label>
								@if (isset($showTicket) && \Gate::denies('it-staff'))
								<input type="text" class="form-control" name="" readonly value="{{ isset($showTicket->requestType) ? $showTicket->requestType->category : 'To be filled up by IT Staff' }}">
								@else
								<select class="form-control" name="category" disabled {{ isset($updateTicket) || isset($itSupportApproval) ? 'disabled' : '' }}>
									<option value="">Select Category</option>
								</select>
								@endif
								@if($errors->has('category'))
								<span id="helpBlock2" class="help-block">{{ $errors->first('category') }}</span>
								@endif
							</div>

							<div class="form-group {{ $errors->has('priority_level') ? 'has-error' : '' }}">
								<label for="priority_level">Priority Level</label>
								@if (isset($showTicket) && \Gate::denies('it-staff'))
								<input type="text" class="form-control" name="" readonly value="{{ isset($showTicket->requestType) ? $showTicket->priorityLevel->level : 'To be filled up by IT Staff' }}">
								@else
								<select class="form-control" name="priority_level" {{ isset($showTicket) && $showTicket->assigned_to != \Auth::user()->id || isset($itSupportApproval) ? 'disabled' : '' }} >
									@foreach (App\PriorityLevel::all() as $level)
									<option value="{{ $level->id }}">{{ $level->level }}</option>
									@endforeach
								</select>
								@endif
								@if($errors->has('priority_level'))
								<span id="helpBlock2" class="help-block">{{ $errors->first('priority_level') }}</span>
								@endif
								@if (!isset($showTicket) && !isset($showTicket))
								<small><b style="color: #ffcccc">Low</b> - Minor loss of functionality, any new service/offerings requests, how-to questions. <br><b style="color: #ff9999">Normal</b> - Moderate loss of functionality or performance resulting in minor performance degradation/not impacting production. Any request which is important but do not require action to be taken on high priority. <br><b style="color: #ff6666">High</b> - Any request which is important but may not require immediate action such that it is not business critical/urgent. <br><b style="color: #cc0000">Urgent</b> - Any request which is important and require immediate action as it is business critical / urgent <br><b style="color: #990000">Immediate</b> - Blocks functionality or delivery of task</small>
								@endif
							</div>
						</div>
						

						@if ((!isset($showTicket) && !isset($updateTicket)) || \Gate::allows('it-staff'))
							<div class="form-group supervisor">
								
							</div>
						@endif

						@if (isset($showTicket))
						<div class="form-group">
							<label>Status</label>
							<input type="text" class="form-control" name="" value="{{ $showTicket->statusChild->status }}" readonly>
						</div>

							@if (Gate::denies('requestor'))
								<div class="form-group">
									<label>Assigned To</label>
									<input type="text" class="form-control" name="" value="{{ $showTicket->userChild->fullNameById($showTicket->assigned_to) ? $showTicket->userChild->fullNameById($showTicket->assigned_to) : 'Pending' }}" readonly>
								</div>
							@endif
						@endif

						<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
							<label for="description">Description</label>
							<textarea class="form-control" name="description" {{ isset($showTicket) ? 'readonly' : '' }}>{{ isset($showTicket) ? $showTicket->description : '' }}{{ isset($updateTicket) ? $updateTicket->description : '' }}</textarea>
							<span id="charlimit" class="help-block pull-right">500 Characters left</span>
							@if($errors->has('description'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('description') }}</span>
							@endif
						</div>

						@if (isset($showTicket) || isset($updateTicket))
						<div class="form-group">
							<table class="table table-striped table-hover table-bordered text-center">
								<thead>
									<tr>
										<th>Updated By</th>
										<th>Remarks</th>
										<th>Status</th>
										<th>Date</th>
									</tr>
								</thead>
								<tbody>
									@foreach (isset($showTicket) ? $showTicket->requestStatusTracker : $updateTicket->requestStatusTracker as $status)
									<tr>
										<td>{{ $status->new_status == 13 ? 'IT Staff' : App\User::find($status->updated_by)->fullName() }}</td>
										<td><span data-toggle="tooltip" title="{{ $status->remarks }}">{{ strlen($status->remarks) > 10 ? substr($status->remarks, 0, 10). '...' : $status->remarks }}</span></td>
										<td>{{ App\Status::find($status->new_status)->status }}</td>
										<td>{{ $status->updated_at }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						@endif
					</div>
					<!-- /.box-body -->

					@if (isset($showTicket))
						@if (\Gate::allows('access-matrix', 24) || \Gate::allows('it-head'))
							@if (isset($approver) && ($showTicket->status == 11 || $showTicket->status == 12))
							<div class="box-footer">
								<div class="text-center">
									<button class="btn btn-success approverBtn" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Loading.." data-status='7' data-id='{{ $showTicket->id }}'>Approve</button>
									<button class="btn btn-danger approverBtn" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Loading.." data-status='8' data-id='{{ $showTicket->id }}'>Disapprove</button>
								</div>
							</div>
							@endif
						@elseif(\Gate::allows('it-staff') && $showTicket->assigned_to == \Auth::user()->id && !isset($itSupportApproval))
						<div class="box-footer">
							<div class="text-center">
								<input type="submit" id="_submit" hidden>
								<button name="submit" class="btn btn-success approverBtn" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Loading..">Submit</button>
							</div>
						</div>
						@endif

					@else
					<div class="box-footer">
						<div class="text-center">
							<input type="submit" id="_submit" hidden>
							<button name="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Loading..">{{ isset($updateTicket) ? 'Update' : 'Open' }} Ticket</button>
							<button name="cancel" class="btn btn-default">Discard</button>
						</div>
					</div>
					@endif
				</div>
			</div>
		</form>
	</div>
@stop

@section('js')
	<script type="text/javascript">
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();
			@if(session()->has('ticketUpdated'))
			$('.top-right').notify({
				message: { text: "Ticket updated successfully." }
			}).show();
			@endif

			@if (session()->has('ticketCreated'))
			$('.top-right').notify({
				message: { text: "Ticket Created successfully." }
			}).show();
			@endif

			@if ($errors->has('supervisor'))
				swal('Oops..', 'Supervisor does not exist.', 'error');
			@endif

			@if(!isset($showTicket) || \Gate::allows('it-staff'))

			$('[name="submit"]').on('click', function(e){
				e.preventDefault();
				swal({
					type: 'question',
					text: 'Are you sure you want to {{ isset($updateTicket) || \Gate::allows('it-staff') ? 'update this' : 'create a' }} ticket?',
					showConfirmButton: true,
					showCancelButton: true,
					confirmButtonText: 'Yes',
					cancelButtonText: 'No',
					showLoaderOnConfirm: true
				}).then(function(){
					$('[name="submit"]').button('loading');
					$('#_submit').click();
				});
			})

			$(document).on('change', 'select[name="category"]', function(){
				var type = $('select[name="type"]').val();
				var category = $(this).val();
				if(category == null){
					console.log('test')
					$('.supervisor').children().remove();
				}else{
					$.ajax({
						url: '{{ route('select-request-type') }}',
						type: 'POST',
						data: { type: type, category: category },
						success: function(data, result){
							if(data == '1'){
								$('.supervisor').append(
										'<label>Supervisor\'s Email</label>'+
										'<select id="supervisor" name="supervisor" class="form-control" @if (isset($showTicket) && !in_array($showTicket->status, [11, 12, 5, 9, 13])) disabled @elseif(isset($showTicket) && $showTicket->assigned_to == null) disabled @elseif(isset($showTicket) && in_array($showTicket->status, [11,12])) disabled @else @endif>'+
											'<option @if (isset($showTicket) && !in_array($showTicket->status, [11, 12, 5, 9])) selected @endif></option>'+
											@foreach (App\User::all() as $user)
											'<option value="{{ $user->id }}">{{ $user->email }}</option>'+
											@endforeach
										'</select>'
									);

								$('#supervisor').select2({
									placeholder: 'Select supervisor'
								});

								@if (isset($showTicket->assignedTo))
								var option = new Option('{{ $showTicket->assignedTo->email }}', '{{ $showTicket->assigned_to }}', true, true);
								$('#supervisor').append(option).trigger('change');
								$('#supervisor').trigger({
									type: 'select2:select',
									params: {
										data: '{{ $showTicket->assigned_to }}'
									}
								});
								@endif
							}else{
								$('.supervisor').children().remove();
							}
						}
					});
				}
			})

			$(document).on('change', 'select[name="type"]', function(){
				$('select[name="category"]').empty();
				$('select[name="category"]').append('<option value="">Select Category</option>');
				if($(this).val() != ''){
					$('select[name="category"]').empty();
					@if (!isset($updateTicket))
					$('select[name="category"]').removeAttr('disabled');
					@endif
					$.ajax({
						url: '{{ route('select-category') }}',
						type: 'POST',
						data: {id: $(this).val()},
						dataType: 'JSON',
						success: function(data, status){
							$.each(data, function(index, val) {
								$('select[name="category"]').append('<option value="'+ val.category +'">' + val.category + '</option>');
							});
							$('select[name="category"]').append('<option value="Other">Other</option>');

							$('select[name="category"]').trigger('change');
						}
					});
				}else{
					$('select[name="category"]').attr('disabled', 'disabled');
				}
			})
			@else
				@if (\Gate::allows('access-matrix', 24) || \Gate::allows('it-head'))
					@if (isset($approver) && ($showTicket->status == 11 || $showTicket->status == 12))
					$(document).on('click', '.approverBtn', function(e){
						e.preventDefault();
						var id = $(this).data('id');
						var status_id = $(this).data('status');
						var type = "";
						var remarks = "";
						switch(status_id){
							case 7:
								type = "Approve";
								break;
							case 8:
								type = "Disapprove";
								break;
							default:
								swal('Error', 'Invalid Request, reloading page.', 'error').then(function(){
									window.location.reload();
								});
								break;
						}
						if(status_id == 8){
							console.log('8');
							swal({
								title: 'Disapproval reason',
								text: 'Enter reason for disapproval',
								input: 'textarea',
								showCancelButton: true,
								confirmButtonText: 'Proceed',
								type: 'question',
								showLoaderOnConfirm: true
							}).then(function(result){
								swal({
									text: 'Are you sure you want to '+ type +' this Ticket?',
									showCancelButton: true,
									cancelButtonText: 'No',
									confirmButtonText: 'Yes',
									type: 'question',
									showLoaderOnConfirm: true
								}).then(function(){
									$.ajax({
										url: '../approvals/' + id,
										type: 'PATCH',
										data: {approver: true, status: status_id, remarks: result},
										dataType: 'JSON',
										success: function(data, result){
											swal(data.title, data.msg, data.type).then(function(){
												window.location.href = "{{ action('ApprovalController@index') }}";
											});
										}
									});
								});
							});
						}else{
							swal({
								text: 'Are you sure you want to '+ type +' this Ticket?',
								showCancelButton: true,
								cancelButtonText: 'No',
								confirmButtonText: 'Yes',
								type: 'question',
								showLoaderOnConfirm: true
							}).then(function(){
								$.ajax({
									url: '../approvals/' + id,
									type: 'PATCH',
									data: {approver: true, status: status_id},
									dataType: 'JSON',
									success: function(data, result){
										swal(data.title, data.msg, data.type).then(function(){
											window.location.href = "{{ action('ApprovalController@index') }}";
										});
									}
								});
							});
						}
					})
					@endif
				@endif
			@endif

			@if (isset($updateTicket))
			$('textarea[name="description"]').val('{{ $updateTicket->description }}');
			@elseif(\Gate::allows('it-staff') && !isset($itSupportApproval) && isset($showTicket))
			$('select[name="type"]').val('{{ isset($showTicket->requestType) ? $showTicket->requestType->type : '' }}');
			$('select[name="type"]').trigger('change');
			
			$('select[name="category"]').val('{{ isset($showTicket->requestType) ? $showTicket->requestType->category : '' }}');
			$('select[name="priority_level"]').val('{{ $showTicket->priority_level }}');
			@endif
		});

		$(document).on('keyup', '[name="description"]', function(){
			var limit = 500;
			var desc = $(this).val();
			var total = limit - desc.length;
			$('#charlimit').text(total + ' Characters left');
			if(total < 1){
				$(this).val(desc.substring(0, 500));

				$('#charlimit').text('0 Characters left');
			}
		})

		$(document).on('click', '[name="cancel"]', function(e){
			e.preventDefault();
			swal({
				type: 'question',
				text: 'Are you sure you want to discard?',
				showConfirmButton: true,
				showCancelButton: true,
				confirmButtonText: 'Yes',
				cancelButtonText: 'No',
				showLoaderOnConfirm: true
			}).then(function(){
				window.location.href = '@if(\Gate::allows('it-staff')) {{ action('ItSupportTicketController@index') }} @elseif(\Gate::allows('approver')) {{ action('TicketController@index') }} @else {{ action('TicketController@index') }} @endif';
			});
		})
	</script>
@stop