<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessMatrix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_matrix', function (Blueprint $table) {
            $table->increments('id');
            $table->string('module');
            $table->string('submodule');
            $table->string('action');
            $table->integer('requestor');
            $table->integer('approver');
            $table->integer('it_staff');
            $table->integer('it_head');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_matrix');
    }
}
