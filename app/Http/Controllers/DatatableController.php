<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\RequestType;
use App\Email;
use App\Resolution;
use DataTables;

class DatatableController extends Controller
{
	public function tickets(){
		$builder = Ticket::query()
		->leftJoin('request_type', 'requests.request_type', 'request_type.id')
		->leftJoin('priority_levels', 'requests.priority_level', 'priority_levels.id')
		->leftJoin('type', 'request_type.type', 'type.id')
		->join('status', 'requests.status', 'status.id')
		->leftJoin('tjsg_hris.employee_details', 'requests.assigned_to', 'employee_details.id')
		->join('tjsg_hris.employee_details AS ed', 'requests.requested_by', 'ed.id')
		->select('requests.ticket_no', 'type.title AS type', 'request_type.category AS category', 'priority_levels.level AS priority_level', 'status.status AS status', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = requests.assigned_to) AS assigned_to'), 'requests.requested_by', 'requests.id', 'requests.status AS status_id', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = requests.requested_by) AS created_by'))
		->where('requests.requested_by', \Auth::user()->id)
		->where('requests.status', '!=', 3)
		->orderByRaw('FIELD(requests.status,5,9,13,7,11,12,1,2,10,6,4,8,3)')
		->orderBy('priority_levels.id', 'DESC')
		->orderBy('requests.id', 'DESC');
		return datatables()->eloquent($builder)->toJson();
	}

	public function approvals(){
		$builder = Ticket::query()
		->join('request_type', 'requests.request_type', 'request_type.id')
		->join('priority_levels', 'requests.priority_level', 'priority_levels.id')
		->join('type', 'request_type.type', 'type.id')
		->join('status', 'requests.status', 'status.id')
		->leftJoin('tjsg_hris.employee_details', 'requests.assigned_to', 'employee_details.id')
		->join('tjsg_hris.employee_details AS ed', 'requests.requested_by', 'ed.id')
		->leftJoin('requests_status_tracker', 'requests.assigned_to', 'requests_status_tracker.updated_by')
		->select('requests.ticket_no', 'type.title AS type', 'request_type.category AS category', 'priority_levels.level AS priority_level', 'status.status AS status', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = requests.assigned_to) AS assigned_to'), 'requests.requested_by', 'requests.id', 'requests.status AS status_id', 'requests.description',  \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = requests.requested_by) AS created_by'), 'requests.assigned_to AS assigned_id')
		->where('requests.status', '!=', 3)
		->where('requests.status', 11)
		->orWhere('requests.assigned_to', \Auth::user()->id)
		->distinct('requests_status_tracker.updated_by')
		->orderBy('priority_levels.id', 'DESC')
		->orderBy('requests.id', 'DESC');
		return datatables()->eloquent($builder)->toJson();
	}

	public function itSupportTickets(){
		$builder = Ticket::query()
		->leftJoin('request_type', 'requests.request_type', 'request_type.id')
		->leftJoin('priority_levels', 'requests.priority_level', 'priority_levels.id')
		->leftJoin('type', 'request_type.type', 'type.id')
		->join('status', 'requests.status', 'status.id')
		->leftJoin('tjsg_hris.employee_details', 'requests.assigned_to', 'employee_details.id')
		->join('tjsg_hris.employee_details AS ed', 'requests.requested_by', 'ed.id')
		->leftJoin('requests_status_tracker', 'requests.id', 'requests_status_tracker.request', 'right')
		->select('requests.ticket_no', 'type.title AS type', 'request_type.category AS category', 'priority_levels.level AS priority_level', 'status.status AS status', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = requests.assigned_to) AS assigned_to'), 'requests.requested_by', 'requests.id', 'requests.status AS status_id', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = requests.requested_by) AS created_by'), 'requests.description', 'request_type.holdable', \DB::raw('(SELECT old_status FROM requests_status_tracker WHERE request = requests.id ORDER BY id DESC LIMIT 1) AS old_status'), 'requests.request_type AS request_type')
		->whereIn('requests.status', [5,7,9,1,13])
		->distinct('requests_status_tracker.request')
		->orderBy('priority_levels.id', 'DESC')
		->orderBy('requests.id', 'DESC');
		return datatables()->eloquent($builder)->toJson();
	}

	public function itSupportApprovals(){
		$builder = Ticket::query()
		->join('request_type', 'requests.request_type', 'request_type.id')
		->join('priority_levels', 'requests.priority_level', 'priority_levels.id')
		->join('type', 'request_type.type', 'type.id')
		->join('status', 'requests.status', 'status.id')
		->leftJoin('tjsg_hris.employee_details', 'requests.assigned_to', 'employee_details.id')
		->join('tjsg_hris.employee_details AS ed', 'requests.requested_by', 'ed.id')
		->leftJoin('requests_status_tracker', 'requests.assigned_to', 'requests_status_tracker.updated_by')
		->select('requests.ticket_no', 'type.title AS type', 'request_type.category AS category', 'priority_levels.level AS priority_level', 'status.status AS status', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = requests.assigned_to) AS assigned_to'), 'requests.requested_by', 'requests.id', 'requests.status AS status_id', 'requests.description',  \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE id = requests.requested_by) AS created_by'), 'requests.assigned_to AS assigned_id')
		->whereIn('requests.status', [7,8,11])
		->distinct('requests_status_tracker.updated_by')
		->orderBy('priority_levels.id', 'DESC')
		->orderBy('requests.id', 'DESC');
		return datatables()->eloquent($builder)->toJson();
	}

	public function requestSelection(){
		$builder = RequestType::query()
		->join('type', 'request_type.type', 'type.id')
		->select('type.title AS type', 'request_type.category AS category', 'request_type.status AS status', 'request_type.requiresApproval AS requiresApproval', 'request_type.holdable AS holdable', 'request_type.hold_time AS hold_time', 'request_type.target_sla AS target_sla', 'request_type.id AS id')
		->orderBy('request_type.id', 'DESC');
		return datatables()->eloquent($builder)->toJson();
	}

	public function emailNotif(){
		$builder = Email::query()
		->join('status', 'email_notification.request_status', 'status.id')
		->select('email_notification.subject', 'status.status AS request_status', 'email_notification.body', 'email_notification.status', 'status.id AS status_id', 'email_notification.id');
		return datatables()->eloquent($builder)->toJson();
	}

	public function resolutionCode(){
		$builder = Resolution::query()->orderBy('id', 'DESC');
		return datatables()->eloquent($builder)->toJson();
	}
}
