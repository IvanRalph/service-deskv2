<?php

namespace App\Http\Middleware;

use Closure;

class requestor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Gate::allows('requestor')){
            return $next($request);
        }

        return redirect('/');
    }
}
