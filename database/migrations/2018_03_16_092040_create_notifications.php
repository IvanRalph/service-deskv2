<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id')->unsigned();
            $table->longText('notification');
            $table->integer('isRead')->default(0);
            $table->integer('user_id')->unsigned();
            $table->timestamp('created_at');

            $table->foreign('request_id')
            ->references('id')->on('requests')
            ->onUpdate('cascade');

            if (App::environment('production')) {
                $table->foreign('user_id')
                ->references('id')->on('ltxx_systemadmin.users')
                ->onUpdate('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
