<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailNotificationAuditTrail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_notification_audit_trail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('email_notif')->unsigned();
            $table->string('field');
            $table->longText('old_value');
            $table->longText('new_value');
            $table->integer('action_by')->unsigned();
            $table->timestamp('action_date');

            $table->foreign('email_notif')
            ->references('id')->on('email_notification')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_notification_audit_trail');
    }
}
