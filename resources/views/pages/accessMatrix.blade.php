@extends('adminlte::page')

@section('title', 'Access Matrix | IT Service Desk')

@section('content_header')
    <h1>Access Matrix</h1>
@stop

@section('content')
	<div class='notifications top-right'></div>
	<form action="{{ action('AccessMatrixController@store') }}" method="POST">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-md-12">
				<table id="accessMatrix" class="table table-striped table-hover table-bordered text-center">
					<thead>
						<tr>
							<th>Module</th>
							<th>Submodule</th>
							<th>Action</th>
							<th>Requestor</th>
							<th>Approver</th>
							<th>IT Staff</th>
							<th>IT Head</th>
						</tr>
					</thead>	
					<tbody>
						@foreach (App\SystemAccessMatrix::get() as $access)
						<tr class="@if($access->id == 1) active @elseif($access->id <= 9) success @elseif($access->id <= 13) info @elseif($access->id <= 21) warning @else danger @endif" title="{{ $access->id }}">
							<td>{{ $access->module }}</td>
							<td>{{ $access->submodule }}</td>
							<td>{{ $access->action }}</td>
							<td><input type="checkbox" name="req_{{ $access->id }}" {{ $access->requestor == 1 ? 'checked' : '' }}></td>
							<td><input type="checkbox" name="app_{{ $access->id }}" {{ $access->approver == 1 ? 'checked' : '' }}></td>
							<td><input type="checkbox" name="it_{{ $access->id }}" {{ $access->it_staff == 1 ? 'checked' : '' }}></td>
							<td><input type="checkbox" name="head_{{ $access->id }}" {{ $access->it_head == 1 ? 'checked' : '' }}></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="row col-md-offset-3">
			<div class="col-md-4 col-sm-12">
				<input type="submit" name="submit" data-loading="Loading..." class="btn btn-primary btn-block" value="SAVE">
			</div>
			<div class="col-md-4 col-sm-12">
				<a href="/" class="btn btn-default form-control">CANCEL</a>
			</div>
		</div>
	</form>
@stop

@section('js')
	<script type="text/javascript">
		$(document).on('click', '#accessMatrix td', function(){
			if($(this).find('input').attr('checked')){
				$(this).find('input').removeAttr('checked');
			}else{
				$(this).find('input').attr('checked', 'checked');
			}
		});

		$('input[name="submit"]').on('click', function(){
			$(this).button('loading');
		})
	</script>
@stop