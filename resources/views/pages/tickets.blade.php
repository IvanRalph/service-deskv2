@extends('adminlte::page')

@section('title', 'Tickets | IT Service Desk')

@section('content_header')
    <h1>Tickets</h1>
@stop

@section('css')
	<style type="text/css">
		.dataTables_scrollBody{
			overflow:visible!important;
		}
	</style>
@stop

@section('content')
	<div class='notifications top-right'></div>
	@isset ($requestorTicket)
	<table id="requestorTable" class="table table-striped table-hover table-bordered text-center">
		<thead>
			<tr>
				<th>Ticket No.</th>
				<th>Type</th>
				<th>Category</th>
				<th>Priority</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
	</table>
	@else
	<table id="itSupportTable" class="table table-striped table-hover table-bordered text-center">
		<thead>
			<tr>
				<th>Ticket No.</th>
				<th>Type</th>
				<th>Category</th>
				<th>Priority</th>
				<th>Description</th>
				<th>Requestor</th>
				<th>Assigned To</th>
				<th>Action</th>
			</tr>
		</thead>	
	</table>
	@endisset
@stop

@section('js')
	<script type="text/javascript">
		@isset ($requestorTicket)
		{{-- ================================R E Q U E S T O R================================ --}}
		var table = $('#requestorTable').DataTable({
			processing: true,
			'order': [],
			ajax: '/datatable/tickets',
			columns: [
				{ data: 'ticket_no', width: '80px' },
				{ data: 'type', width: '20%' },
				{ data: 'category', width: '20%' },
				{ data: 'priority_level', width: '100px', sortable: false },
				{ data: 'status', width: '100px' },
				{ data: null, width: '100%', searchable: false, sortable: false }
			],
			dom: '<"pull-right"B><"pull-left"l {{ \Gate::allows('access-matrix', 21) ? 'f' : '' }} >tip',
			lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
			pageLength: 10,
			columnDefs: [
			{
				targets: -1,
				render: function(a, b, data, d){
					var btn = '';
					if(data.status_id == 2){
						btn += "<a href='#' data-id='"+ data.id +"' class='btn btn-default reopen'>Reopen</a>";
					}
					if(data.requested_by == {{ \Auth::user()->id }} && (data.status_id == 5 || data.status_id == 9 || data.status_id == 11)){
						@if (\Gate::allows('access-matrix', 16))
						btn += "<a href='/tickets/"+ data.id +"/edit' class='btn btn-default'>Update</a>";
						@endif
						@if (\Gate::allows('access-matrix', 17))
						btn += "<a href='#' data-id='"+ data.id +"' class='btn btn-default deleteBtn'>Delete</a>";
						@endif
					}

					return btn;
				}
			},
			{
				targets: [1,2,5],
				render: function(data, type, row){
					if(data == null){
						return 'Pending';
					}

					return data;
				}
			},
			{
				targets: [3],
				render: function(data, type, row){
					switch(data){
						case 'Low':
							return '<b style="color: #ffcccc">'+ data +'</b>';
							break;
						case 'Normal':
							return '<b style="color: #ff9999">'+ data +'</b>';
							break;
						case 'High':
							return '<b style="color: #ff6666">'+ data +'</b>';
							break;
						case 'Urgent':
							return '<b style="color: #cc0000">'+ data +'</b>';
							break;
						case 'Immediate':
							return '<b style="color: #990000">'+ data +'</b>';
							break;
						default:
							return 'Pending';
					}
				}
			},
			{
				targets: [4],
				render: function(data, type, row){
					if(data == null){
						return 'Pending';
					}
					var color = "";
					switch(row.status_id){
						case '1':
							color = 'gray';
							break;
						case '2':
							color = 'primary';
							break;
						case '4': 
							color = 'red';
							break;
						case '5':
							color = 'green';
							break;
						case '6':
							color = 'gray';
							break;
						case '7':
							color = 'green';
							break;
						case '8':
							color = 'red';
							break;
						case '9':
							color = 'green';
							break;
						case '10':
							color = 'maroon';
							break;
						case '11':
						case '12':
							color = 'orange';
							break;
						case '13':
							color = 'navy';
							break;
						default:
							color = 'black';
							break;
					}


					return '<span class="label bg-'+ color +'">'+data+'</span>';
				}
			},
			],
			buttons: [
			@if (\Gate::allows('access-matrix', 20))
			{
				extend: 'excel',
				text: 'Export',
				exportOptions: {
					columns: [0,1,2,3,4]
				}
			},
			@endif
			@if (\Gate::allows('access-matrix', 18))
			{
				text: 'Open Ticket',
				action: function ( e, dt, node, config ) {
					window.location.href="/tickets/create";
				}
			}
			@endif
			],
			"scrollX": true,
			"fixedHeader": true
		});

		@if (\Gate::allows('access-matrix', 17))
		$(document).on('click', '.deleteBtn', function(e){
			e.preventDefault();
			swal({
				text: 'Are you sure you want to delete this Ticket?',
				showCancelButton: true,
				cancelButtonText: 'No',
				confirmButtonText: 'Yes',
				type: 'question',
				showLoaderOnConfirm: true
			}).then(function(){
				$.ajax({
					url: '../tickets/' + $('.deleteBtn').data('id'),
					type: 'DELETE',
					dataType: 'JSON',
					success: function(data, result){
						swal(data.title, data.msg, data.type).then(function(){
							table.ajax.reload();
						});
					}
				});
			});
		})
		@endif

		@if (\Gate::allows('access-matrix', 15))
		$('#requestorTable tbody').on( 'click', 'tr td:not(td:last-child)', function () {
			window.location.href = "/tickets/"+ table.row( this ).data().id;
		} );

		$('#requestorTable tbody').on( 'mouseover', 'tr td:not(td:last-child)', function () {
			$(this).css( 'cursor', 'pointer' );
		} );
		@endif

		@if (\Gate::allows('access-matrix', 19))
			$(document).on('click', '.reopen', function(e){
				e.preventDefault();
				var id = $(this).data('id');

				swal({
					title: 'Reopen reason',
					text: 'Enter reason for reopening',
					input: 'textarea',
					showCancelButton: true,
					confirmButtonText: 'Proceed',
					type: 'question',
					showLoaderOnConfirm: true
				}).then(function(result){
					swal({
						text: 'Are you sure you want to Reopen this Ticket?',
						showCancelButton: true,
						cancelButtonText: 'No',
						confirmButtonText: 'Yes',
						type: 'question'
					}).then(function(){
						$.ajax({
							url: '../tickets/' + id,
							type: 'PATCH',
							data: {reopen: true, remarks: result},
							dataType: 'JSON',
							success: function(data, result){
								swal(data.title, data.msg, data.type).then(function(){
									table.ajax.reload();
								});
							}
						});
					});
				});
			})
		@endif
		{{-- =============================E N D  O F  R E Q U E S T O R============================= --}}

		@else
		{{-- ================================I T  S U P P O R T================================ --}}
		var table = $('#itSupportTable').DataTable({
			processing: true,
			'order': [],
			ajax: '{{ route('it-staff-datatable') }}',
			columns: [
				{ data: 'ticket_no', width: '50px' },
				{ data: 'type', width: '50%' },
				{ data: 'category', width: '50%' },
				{ data: 'priority_level', width: '80px', sortable: false },
				{ data: 'description', width: '100%' },
				{ data: 'created_by', width: '100px' },
				{ data: 'assigned_to', width: '100px' },
				{ data: null, width: '250px', searchable: false, sortable: false }
			],
			dom: '<"pull-right"B><"pull-left"l {{ \Gate::allows('access-matrix', 9) ? 'f' : '' }} >tip',
			lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
			pageLength: 10,
			columnDefs: [
			{
				targets: -1,
				className: 'text-left',
				render: function(a, b, data, d){
					var btn = '';
					@if (\Gate::allows('access-matrix', 5))
					if(data.assigned_to == '{{ \Auth::user()->fullName() }}' && data.request_type  != null){
						btn += "<a href='#' data-id='"+ data.id +"' data-ticket='"+ data.ticket_no +"' class='btn btn-default btn-sm resolveBtn'>Resolve</a>";
					}
					@endif
					@if (\Gate::allows('access-matrix', 4))
					btn += '<div class="btn-group assignBtn">'+
							  '<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
							    'Assign <span class="caret"></span>'+
							  '</button>'+ 
							  '<ul class="dropdown-menu dropdown-menu-right">'+
							    @foreach (App\User::itStaff() as $it)
							    	'<li data-id="{{ $it->id }}" data-tic="'+ data.id +'" data-ticket="'+ data.ticket_no +'""><a href="#">{{ $it->full_name }}</a></li>'+
							    @endforeach
							  '</ul>'+
							'</div>';
					@endif
					@if (\Gate::allows('access-matrix', 7))
					if(data.holdable == 1 && data.assigned_to == '{{ \Auth::user()->fullName() }}'){
						if(data.status_id != 1){
							btn += "<a href='#' data-id='"+ data.id +"' data-hold='1' data-ticket='"+ data.ticket_no +"' class='btn btn-default btn-sm holdBtn'>Hold</a>";
						}else{
							btn += "<a href='#' data-id='"+ data.id +"' data-hold='"+ data.old_status +"' data-ticket='"+ data.ticket_no +"' class='btn btn-default btn-sm holdBtn'>Resume</a>";
						}
					}
					@endif
					@if (\Gate::allows('access-matrix', 6))
					if(data.assigned_to == '{{ \Auth::user()->fullName() }}'){
						btn += "<a href='#' data-id='"+ data.id +"' class='btn btn-default btn-sm cancelBtn' title='Cancel Ticket'><i class='fa fa-times text-red' style='font-size: 100%;'></i></a>";
					}
					@endif
					return btn;
				}
			},
			{
				targets: [6],
				render: function(data, type, row){
					if(data == null){
						return 'Pending';
					}

					return data;
				}
			},
			{
				targets: [3],
				render: function(data, type, row){
					switch(data){
						case 'Low':
							return '<b style="color: #ffcccc">'+ data +'</b>';
							break;
						case 'Normal':
							return '<b style="color: #ff9999">'+ data +'</b>';
							break;
						case 'High':
							return '<b style="color: #ff6666">'+ data +'</b>';
							break;
						case 'Urgent':
							return '<b style="color: #cc0000">'+ data +'</b>';
							break;
						case 'Immediate':
							return '<b style="color: #990000">'+ data +'</b>';
							break;
						default:
							return data;
					}
				}
			},
			{
				targets: [4],
				render: function(data, type, row){
					if(data != null){
						return data.length > 10 ?
						'<span data-toggle="tooltip" title="' + data + '">' + data.substr( 0, 10 ) + '...' + '</span>':
						data;
					}else{
						return data;
					}
				}
			},
			],
			buttons: [
			@if (\Gate::allows('access-matrix', 8))
			{
				extend: 'excel',
				text: 'Export',
				exportOptions: {
					columns: [0,1,2,3,4,5]
				}
			}
			@endif
			],
			"scrollX": true,
			"fixedHeader": true
		});

		@if (\Gate::allows('access-matrix', 6))
		$(document).on('click', '.cancelBtn', function(e){
			e.preventDefault();
			var id = $(this).data('id');

			swal({
				title: 'Cancel reason',
				text: 'Enter reason for Cancelling',
				input: 'textarea',
				showCancelButton: true,
				confirmButtonText: 'Proceed',
				type: 'question',
				showLoaderOnConfirm: true
			}).then(function(result){
				swal({
					text: 'Are you sure you want to Cancel this Ticket?',
					showCancelButton: true,
					cancelButtonText: 'No',
					confirmButtonText: 'Yes',
					type: 'question',
					showLoaderOnConfirm: true
				}).then(function(){
					$.ajax({
						url: '../it-staff/tickets/' + id,
						type: 'PATCH',
						data: {cancel: true, remarks: result},
						dataType: 'JSON',
						success: function(data, result){
							swal(data.title, data.msg, data.type).then(function(){
								table.ajax.reload();
							});
						}
					});
				});
			});
		})
		@endif

		$('#itSupportTable').on('draw.dt', function () {
	        $('[data-toggle="tooltip"]').tooltip();
	    });

		@if (\Gate::allows('access-matrix', 3))
			$('#itSupportTable tbody').on( 'click', 'tr td:not(td:last-child)', function () {
				window.location.href = "/it-staff/tickets/"+ table.row( this ).data().id;
			} );

			$('#itSupportTable tbody').on( 'mouseover', 'tr td:not(td:last-child)', function () {
				$(this).css( 'cursor', 'pointer' );
			} );
		@endif

		@if (\Gate::allows('access-matrix', 4))
		$(document).on('click', '.assignBtn li', function(e){
			e.preventDefault();
			var id = $(this).data('tic');
			var userId = $(this).data('id');
			var ticketNo = $(this).data('ticket');
			var staff = $(this).text();
			swal({
				text: 'Are you sure you want to assign Ticket ID '+ ticketNo +' to '+ staff +'?',
				showCancelButton: true,
				cancelButtonText: 'No',
				confirmButtonText: 'Yes',
				type: 'question',
				showLoaderOnConfirm: true
			}).then(function(){
				$.ajax({
					url: '../it-staff/tickets/' + id,
					type: 'PATCH',
					data: {assign: true, staff: userId},
					dataType: 'JSON',
					success: function(data, result){
						swal(data.title, data.msg, data.type).then(function(){
							table.ajax.reload();
							$("#notif-nav").load(location.href + " #notif-nav");
						});
					}
				});
			});
		});
		@endif

		$('#mainTable').on('draw.dt', function () {
	        $('[data-toggle="tooltip"]').tooltip();
	    });

		@if (\Gate::allows('access-matrix', 7))
		$(document).on('click', '.holdBtn', function(e){
			e.preventDefault();
			var id = $(this).data('id');
			var status = $(this).data('hold');
			var type = "";
			var remarks = "";
			switch(status){
				case 1:
					type = "Hold";
					break;
				default:
					type = "Resume";
					break;
			}
			if(status == 1){
				swal({
					title: 'Hold reason',
					text: 'Enter reason for Holding',
					input: 'textarea',
					showCancelButton: true,
					confirmButtonText: 'Proceed',
					type: 'question',
					showLoaderOnConfirm: true
				}).then(function(result){
					swal({
						text: 'Are you sure you want to '+ type +' this Ticket?',
						showCancelButton: true,
						cancelButtonText: 'No',
						confirmButtonText: 'Yes',
						type: 'question',
						showLoaderOnConfirm: true
					}).then(function(){
						$.ajax({
							url: '../it-staff/tickets/' + id,
							type: 'PATCH',
							data: {hold: true, status: status, remarks: result},
							dataType: 'JSON',
							success: function(data, result){
								swal(data.title, data.msg, data.type).then(function(){
									table.ajax.reload();
								});
							}
						});
					});
				});
			}else{
				swal({
					text: 'Are you sure you want to '+ type +' this Ticket?',
					showCancelButton: true,
					cancelButtonText: 'No',
					confirmButtonText: 'Yes',
					type: 'question',
					showLoaderOnConfirm: true
				}).then(function(){
					$.ajax({
						url: '../it-staff/tickets/' + id,
						type: 'PATCH',
						data: {hold: true, status: status, remarks: ''},
						dataType: 'JSON',
						success: function(data, result){
							swal(data.title, data.msg, data.type).then(function(){
								table.ajax.reload();
							});
						}
					});
				});
			}
		})
		@endif

		@if (\Gate::allows('access-matrix', 5))
		$(document).on('click', '.resolveBtn', function(e){
			e.preventDefault();
			var id = $(this).data('id');
			var ticket = $(this).data('ticket');
			var remarks = "";

			swal.setDefaults({
			  input: 'text',
			  confirmButtonText: 'Next &rarr;',
			  showCancelButton: true,
			  progressSteps: ['1', '2']
			})

			var steps = [
			  {
			    title: 'Resolution Code',
			    text: 'Select Resolution Code',
			    input: 'select',
			    inputClass: 'form-control',
			    inputOptions: {
			    	@foreach (App\Resolution::where('status', 1)->get() as $res)
			    		'{{ $res->id }}':'{{ $res->code }}',
			    	@endforeach
			    }
			  },
			  {
			  	title: 'Ticket Resolution',
			  	text: 'Enter resolution for ticket',
			  	input: 'textarea',
			  	inputClass: 'form-control'
			  }
			]

			swal.queue(steps).then((result) => {
				swal.resetDefaults()
				console.log(result[0])
				if (result[0] && result[1]) {
					var code = result[0];
					swal({
						text: 'Are you sure you want to resolve this Ticket?',
						showCancelButton: true,
						cancelButtonText: 'No',
						confirmButtonText: 'Yes',
						type: 'question',
						showLoaderOnConfirm: true
					}).then(function(){
						$.ajax({
							url: '../it-staff/tickets/' + id,
							type: 'PATCH',
							data: {resolve: true, remarks: result[1], resolution_code: code},
							dataType: 'JSON',
							success: function(data, result){
								swal(data.title, data.msg, data.type).then(function(){
									table.ajax.reload();
								});
							}
						});
					});
				}else{
					swal('Oops', 'Please fill up the required fields.', 'warning');
				}
			})
		})	
		@endif
		{{-- =============================E N D  O F  I T  S U P P O R T============================= --}}
		@endisset
	</script>
@stop