<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequestType;

class RequestSelectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.requestSelection');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.createSelection');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'type'=>'required|exists:type,id',
            'category'=>'required|max:191',
            'hold_day'=>'nullable|min:0|max:365',
            'hold_time'=>'nullable|date_format:"H:i:s"',
            'sla_day'=>'nullable|min:0|max:365',
            'sla_time'=>'nullable|date_format:"H:i:s"'
        ]);

        if($validator->fails()){
            $request->flash();
            return redirect(action('RequestSelectionController@create'))
                    ->withErrors($validator);
        }

        if($request->input('holdable') != null){
            $time = explode(':', $request->input('hold_time'));
            $hour = $time[0] + ($request->input('hold_day') * 24);
            $min = $time[1] / 60;
            $holdTime = $hour + $min;
        }

        $time = explode(':', $request->input('sla_time'));
        $hour = $time[0] + ($request->input('sla_day') * 24);
        $min = $time[1] / 60;
        $sla = $hour + $min;

        RequestType::create([
            'type'=>$request->input('type'),
            'category'=>$request->input('category'),
            'requiresApproval'=>$request->input('approvalRequirement') != null ? 1 : 0,
            'holdable'=>$request->input('holdable') != null ? 1 : 0,
            'hold_time'=>isset($holdTime) ? $holdTime : null,
            'target_sla'=>$sla,
            'created_by'=>\Auth::user()->id
        ]);

        return redirect(action('RequestSelectionController@index'))->with('selectionCreated', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.createSelection')->with('updateSelection', RequestType::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->input('activate')){
            $validator = \Validator::make($request->all(), [
                'id'=>'exists:request_type,id',
                'status'=>'in:0,1'
            ]);

            if($validator->fails()){
                \Log::error('Data modified before entry.' ,\Auth::user(), $request->all());
                return ['title'=>'Error', 'msg'=>'There was an error with your request, contact it.enterprise@solarphilippines.ph to clarify.', 'type'=>'error'];
            }

            RequestType::find($id)->update([
                'status'=>$request->input('status')
            ]);

            return ['title'=>'Updated', 'msg'=>'The selection\'s status has been updated.', 'type'=>'success'];
        }

        $validator = \Validator::make($request->all(), [
            'type'=>'required|exists:type,id',
            'category'=>'required',
            'hold_day'=>'nullable|min:0|max:365',
            'hold_time'=>'nullable|date_format:"H:i:s"',
            'sla_day'=>'nullable|min:0|max:365',
            'sla_time'=>'nullable|date_format:"H:i:s"'
        ]);

        if($validator->fails()){
            $request->flash();
            return redirect(action('RequestSelectionController@edit', $id))
                    ->withErrors($validator);
        }

        if($request->input('holdable') != null){
            $time = explode(':', $request->input('hold_time'));
            $hour = $time[0] + ($request->input('hold_day') * 24);
            $min = $time[1] / 60;
            $holdTime = $hour + $min;
        }

        $time = explode(':', $request->input('sla_time'));
        $hour = $time[0] + ($request->input('sla_day') * 24);
        $min = $time[1] / 60;
        $sla = $hour + $min;

        RequestType::find($id)->update([
            'type'=>$request->input('type'),
            'category'=>$request->input('category'),
            'requiresApproval'=>$request->input('approvalRequirement') != null ? 1 : 0,
            'holdable'=>$request->input('holdable') != null ? 1 : 0,
            'hold_time'=>isset($holdTime) ? $holdTime : null,
            'target_sla'=>$sla,
            'created_by'=>\Auth::user()->id
        ]);

        return redirect(action('RequestSelectionController@index'))->with('selectionUpdated', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
