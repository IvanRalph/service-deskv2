<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Ticket;
use App\SystemAccessMatrix;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('requestor', function($user){
            return $user->departmentDetails()->department_id != '20180024IT' ? true : false;
        });

        Gate::define('approver', function($user){
            return Ticket::where([
                'status'=>11,
                'assigned_to'=>$user->id
            ])->first() ? true : false;
        });

        Gate::define('it-staff', function($user){
            return $user->departmentDetails()->department_id == '20180024IT' || $user->itHead()->supervisor == $user->employee_id ? true : false;
        });

        Gate::define('it-head', function($user){
            return $user->itHeadId()->id === \Auth::user()->id;
        });

        Gate::define('access-matrix', function($user, $id){
            $acl = SystemAccessMatrix::where('id', $id)->first();
            if($user->isUserType() == 1){
                return $acl->requestor === 1;
            }elseif($user->isUserType() == 2){
                return $acl->approver === 1;
            }elseif($user->isUserType() == 3){
                return $acl->it_staff === 1;
            }elseif($user->isUserType() == 4){
                return $acl->it_head === 1;
            }
        });
    }
}
