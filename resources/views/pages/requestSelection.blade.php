@extends('adminlte::page')

@section('title', 'Request Selections | IT Service Desk')

@section('content_header')
    <h1>Request Selections</h1>
@stop

@section('content')
	<div class='notifications top-right'></div>
	<table id="selectionTable" class="table table-striped table-hover table-bordered text-center">
		<thead>
			<tr>
				<th>Type</th>
				<th>Category</th>
				<th>Status</th>
				<th>Requires Approval?</th>
				<th>Holdable?</th>
				<th>Max Hold Time</th>
				<th>Target</th>
				<th>Action</th>
			</tr>
		</thead>	
	</table>
@stop

@section('js')
	<script type="text/javascript">
		var table = $('#selectionTable').DataTable({
			processing: true,
			'order': [],
			ajax: '{{ route('setup-request-selection') }}',
			columns: [
				{ data: 'type', width: '80px' },
				{ data: 'category', width: '20%' },
				{ data: 'status', width: '20%' },
				{ data: 'requiresApproval', width: '100px' },
				{ data: 'holdable', width: '50px' },
				{ data: 'hold_time', width: '100px' },
				{ data: 'target_sla', width: '150px' },
				{ data: null, width: '200px', searchable: false, sortable: false }
			],
			dom: 'r<"pull-right"B><"pull-left"lf>tip',
			lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
			pageLength: 10,
			columnDefs: [
			{
				targets: -1,
				render: function(a, b, data, d){
					var btn = '';
					btn += "<a href='/setup/requests-selection/"+ data.id +"/edit' class='btn btn-default'>Update</a>";
					if(data.status == 1){
						btn += "<a href='#' data-status='0' data-id='"+ data.id +"' class='btn btn-default statusBtn'>Deactivate</a>";
					}else{
						btn += "<a href='#' data-status='1' data-id='"+ data.id +"' class='btn btn-default statusBtn'>Activate</a>";
					}
					return btn;
				}
			},
			{
				targets: [6],
				render: function(data, type, row){
					if(data == null){
						return 'TBA';
					}

					return data;
				}
			},
			{
				targets: [5],
				render: function(data, type, row){
					if(data == null){
						return 'N/A';
					}

					return data;
				}
			},
			{
				targets: [2],
				render: function(data, type, row){
					if(data == 1){
						return 'Active';
					}else{
						return 'Inactive';
					}

					return data;
				}
			},
			{
				targets: [3,4],
				render: function(data, type, row){
					if(data == 1){
						return 'Yes';
					}else{
						return 'No';
					}

					return data;
				}
			}
			],
			buttons: [
			{
				extend: 'excel',
				text: 'Export',
				exportOptions: {
					columns: [0,1,2,3,4,5]
				}
			},
			{
				text: 'Create Request Selection',
				action: function ( e, dt, node, config ) {
					window.location.href="{{ action('RequestSelectionController@create') }}";
				}
			}
			],
			"scrollX": true,
			"fixedHeader": true
		});

		$(document).on('click', '.statusBtn', function(e){
			e.preventDefault();
			var status = $(this).data('status');
			var id = $(this).data('id');
			var msg = '';

			if(status == 0){
				msg = 'Deactivate';
			}else{
				msg = 'Activate';
			}

			swal({
				text: 'Are you sure you want to ' + msg + ' this selection?',
				confirmButtonText: 'Yes',
				showCancelButton: true,
				cancelButtonText: 'No',
				type: 'question',
				showLoaderOnConfirm: true
			}).then(function(){
				$.ajax({
					url: '/setup/requests-selection/'+id,
					type: 'PATCH',
					data: { activate: true, id: id, status: status },
					dataType: 'JSON',
					success: function(data, status){
						swal(data.title, data.msg, data.type).then(function(){
							table.ajax.reload();
						});
					}
				})
			})
		})
	</script>
@stop