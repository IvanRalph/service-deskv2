@extends('adminlte::page')

@section('title', 'User Profile | IT Service Desk')

@section('content_header')

@stop

@section('content')
<div class='notifications top-right'></div>
	<div class="row">
		<div class="col-sm-2">
			<a href="{{ url()->previous() }}" style="color: #333; font-size: 35px!important;"><i class="fa fa-arrow-left"></i></a>
		</div>
	</div>
	<div class="row">
		<form action="{{ action('ProfileController@store') }}" method="POST">
			@csrf
			<div class="col-md-6 col-md-offset-3">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-user"></i> User Profile</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="form-group {{ $errors->has('old_password') ? 'has-error' : '' }} ">
							<label for="old_password">Old Password</label>
							<input type="password" name="old_password" id="old_password" class="form-control" placeholder="Old Password">
							@if($errors->has('old_password'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('old_password') }}</span>
							@endif
						</div>

						<div class="form-group {{ $errors->has('new_password') ? 'has-error' : '' }} ">
							<label for="new_password">New Password</label>
							<input type="password" name="new_password" id="new_password" class="form-control" placeholder="New Password">
							@if($errors->has('new_password'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('new_password') }}</span>
							@endif
						</div>

						<div class="form-group">
							<label for="new_password_confirmation">Confirm New Password</label>
							<input type="password" name="new_password_confirmation" id="new_password_confirmation" class="form-control" placeholder="Confirm New Password">
						</div>
					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<div class="text-center">
							<input type="submit" id="_submit" hidden>
							<button name="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Loading..">Change Password</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
@stop

@section('js')
	<script type="text/javascript">
		$(document).ready(function(){
			@if (session('error'))
			$('.top-right').notify({
				message: { text: "{{ session('error') }}" },
				type: 'danger'
			}).show();
			@endif

			@if (session()->has('passwordChanged'))
				$('.top-right').notify({
					message: { text: "Password Changed successfully" },
					type: 'success'
				}).show();
			@endif
		});
	</script>
@stop