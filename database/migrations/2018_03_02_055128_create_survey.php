<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request')->unsigned();
            $table->integer('rate');
            $table->integer('staff')->unsigned();
            $table->timestamp('rate_date');

            $table->foreign('request')
            ->references('id')
            ->on('requests')
            ->onUpdate('cascade');

            

            if (App::environment('production')) {
                $table->foreign('staff')
                ->references('id')
                ->on('tjsg_hris.employee_details')
                ->onUpdate('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey');
    }
}
