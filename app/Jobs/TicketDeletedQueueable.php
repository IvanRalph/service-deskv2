<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\TicketDeleted;

class TicketDeletedQueueable implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $content;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->content['cc'] == ''){
            \Mail::to($this->content['to'])
                ->send(new TicketDeleted($this->content));
        }else{
            \Mail::to($this->content['to'])->cc($this->content['cc'])
                ->send(new TicketDeleted($this->content));
        }
    }
}
