@extends('adminlte::page')

@section('title', 'Resolution Codes | IT Service Desk')

@section('content_header')
    <h1>Resolution Codes</h1>
@stop

@section('content')
	<div class='notifications top-right'></div>
	<table id="resolutionTable" class="table table-striped table-hover table-bordered text-center">
		<thead>
			<tr>
				<th>Code</th>
				<th>Description</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>	
	</table>
@stop

@section('js')
	<script type="text/javascript">
		@if (session()->has('resolutionCreated'))
		$('.top-right').notify({
			message: { text: "Resolution Created successfully." }
		}).show();
		@endif

		@if (session()->has('resolutionUpdated'))
		$('.top-right').notify({
			message: { text: "Resolution Updated successfully." }
		}).show();
		@endif

		var table = $('#resolutionTable').DataTable({
			processing: true,
			'order': [],
			ajax: '{{ route('setup-resolution-code') }}',
			columns: [
				{ data: 'code', width: '100px' },
				{ data: 'description', width: '100%' },
				{ data: 'status', width: '80px' },
				{ data: null, width: '180px', sortable: false, searchable: false },
			],
			dom: 'r<"pull-right"B><"pull-left"lf>tip',
			lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
			pageLength: 10,
			columnDefs: [
			{
				targets: -1,
				render: function(a, b, data, d){
					var btn = '';
					btn += "<a href='/setup/resolution-codes/"+ data.id +"/edit' class='btn btn-default'>Update</a>";
					if(data.status == 1){
						btn += "<a href='#' data-status='0' data-id='"+ data.id +"' class='btn btn-default statusBtn'>Deactivate</a>";
					}else{
						btn += "<a href='#' data-status='1' data-id='"+ data.id +"' class='btn btn-default statusBtn'>Activate</a>";
					}
					return btn;
				}
			},
			{
				targets: [2],
				render: function(data, type, row){
					return data == 1 ? '<span class="label bg-green">Active</span>' : '<span class="label bg-red">Inactive</span>';
				}
			},
			],
			buttons: [
			{
				extend: 'excel',
				text: 'Export',
				exportOptions: {
					columns: [0,1,2]
				}
			},
			{
				text: 'Create Resolution Code',
				action: function ( e, dt, node, config ) {
					window.location.href="{{ action('ResolutionController@create') }}";
				}
			}
			],
			"scrollX": true,
			"fixedHeader": true
		});

		$(document).on('click', '.statusBtn', function(e){
			e.preventDefault();
			var status = $(this).data('status');
			var id = $(this).data('id');
			var msg = '';

			if(status == 0){
				msg = 'Deactivate';
			}else{
				msg = 'Activate';
			}

			swal({
				text: 'Are you sure you want to ' + msg + ' this selection?',
				confirmButtonText: 'Yes',
				showCancelButton: true,
				cancelButtonText: 'No',
				type: 'question',
				showLoaderOnConfirm: true
			}).then(function(){
				$.ajax({
					url: '/setup/resolution-codes/'+id,
					type: 'PATCH',
					data: { activate: true, status: status },
					dataType: 'JSON',
					success: function(data, status){
						swal(data.title, data.msg, data.type).then(function(){
							table.ajax.reload();
						});
					}
				})
			})
		})
	</script>
@stop