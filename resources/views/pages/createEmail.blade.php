@extends('adminlte::page')

@section('title', 'E-Mail Notifications | IT Service Desk')

@section('content_header')

@stop

@section('content')
<div class='notifications top-right'></div>
	<div class="row">
		<div class="col-sm-2">
			<a href="{{ url()->previous() }}" style="color: #333; font-size: 35px!important;"><i class="fa fa-arrow-left"></i></a>
		</div>
	</div>
	<div class="row">
		<form action="{{ action('EmailController@update', $updateEmail->id) }}" method="POST">
			@csrf
			@method('PATCH')
			<div class="col-md-6 col-md-offset-3">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-envelope-o"></i> E-Mail notification</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="form-group {{ $errors->has('subject') ? 'has-error' : '' }} ">
							<label for="subject">Subject</label>
							<input type="text" class="form-control" name="subject" id="subject" value="{{ isset($updateEmail) ? $updateEmail->subject : '' }}">
							@if($errors->has('subject'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('subject') }}</span>
							@endif
						</div>

						<div class="form-group {{ $errors->has('body') ? 'has-error' : '' }} ">
							<label for="body">E-mail Body</label>
							<textarea class="form-control" id="body" name="body" rows="7">{{ isset($updateEmail) ? $updateEmail->body : '' }}</textarea>
							@if($errors->has('body'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('body') }}</span>
							@endif
						</div>
					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<div class="text-center">
							<input type="submit" id="_submit" hidden>
							<button name="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Loading..">Save</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
@stop

@section('js')
	<script type="text/javascript">
		$(document).on('click', 'button[name="submit"]', function(e){
			e.preventDefault();
			swal({
				type: 'question',
				text: 'Are you sure you want to update this e-mail template?',
				showConfirmButton: true,
				showCancelButton: true,
				confirmButtonText: 'Yes',
				cancelButtonText: 'No',
				showLoaderOnConfirm: true
			}).then(function(){
				$('[name="submit"]').button('loading');
				$('#_submit').click();
			});
		})
	</script>
@stop