@extends('adminlte::page')

@section('title', 'Request Selection | IT Service Desk')

@section('content_header')

@stop

@section('content')
<div class='notifications top-right'></div>
	<div class="row">
		<div class="col-sm-2">
			<a href="{{ url()->previous() }}" style="color: #333; font-size: 35px!important;"><i class="fa fa-arrow-left"></i></a>
		</div>
	</div>
	<div class="row">
		<form action="{{ isset($updateSelection) ? action('RequestSelectionController@update', $updateSelection->id) : action('RequestSelectionController@store') }}" method="POST">
			@csrf
			@isset ($updateSelection)
			@method('PATCH')
			@endisset
			<div class="col-md-6 col-md-offset-3">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-list-ul"></i> Create Selection</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
							<label for="type">Type</label>
							<select name="type" id="type" class="form-control">
								<option>Select Type</option>
								@foreach (App\Type::get() as $type)
									<option value="{{ $type->id }}">{{ $type->title }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group {{ $errors->has('category') ? 'has-error' : '' }}">
							<label for="category">Category</label>
							<input type="text" id="category" name="category" class="form-control" placeholder="Enter category">
						</div>

						<div class="col-md-6">
							<div class="form-group {{ $errors->has('holdable') ? 'has-error' : '' }}">
								<label for="holdable">Holdable?</label>
								<input type="checkbox" id="holdable" name="holdable">
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group {{ $errors->has('approvalRequirement') ? 'has-error' : '' }}">
									<label for="approvalRequirement">Requires Approval?</label>
									<input type="checkbox" id="approvalRequirement" name="approvalRequirement">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group col-md-6 holdTime">
								
							</div>

							<div class="form-group col-md-6">
								<label>Target SLA</label>
								<div class="row">
									<div class="col-md-4 {{ $errors->has('sla_day') ? 'has-error' : '' }}">
										<label for="sla_day">Day(s)</label>
										<input type="number" id="sla_day" name="sla_day" class="form-control" value="0" min="0" max="365"> 
									</div>

									<div class="col-md-5 {{ $errors->has('sla_time') ? 'has-error' : '' }}">
										<label for="sla_time">Time</label>
										<input type="text" id="sla_time" name="sla_time" class="form-control" value="00:30:00" placeholder="hh:mm:ss"> 
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<div class="text-center">
							<input type="submit" id="_submit" hidden>
							<button name="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Loading..">Save</button>
							<button name="cancel" class="btn btn-default">Discard</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
@stop

@section('js')
	<script type="text/javascript">
		$(document).ready(function(){
			$("#sla_time").timepicker({
				'timeFormat': 'H:i:s',
				'minTime': '00:30:00',
				'disableTextInput': true
			});
		})

		$(document).on('change', '#holdable', function(){
			if($(this).prop('checked')){
				$('.holdTime').append('<label>Maximum Hold Time</label>'+
								'<div class="row">'+
									'<div class="col-md-4 {{ $errors->has('hold_day') ? 'has-error' : '' }}">'+
										'<label for="hold_day">Day(s)</label>'+
										'<input type="number" id="hold_day" name="hold_day" class="form-control" value="0" min="0" max="365">'+ 
									'</div>'+

									'<div class="col-md-5 {{ $errors->has('hold_time') ? 'has-error' : '' }}">'+
										'<label for="hold_time">Time</label>'+
										'<input type="text" id="hold_time" name="hold_time" class="form-control" value="00:30:00" placeholder="hh:mm:ss">{{ $errors->first('hold_time') }} '+
									'</div>'+
								'</div>');
				$("#hold_time").timepicker({
					'timeFormat': 'H:i:s',
					'minTime': '00:30:00',
					'disableTextInput': true
				});
			}else{
				$('.holdTime').children().remove();
			}
		})

		@isset ($updateSelection)
		    $('#type').val('{{ $updateSelection->type }}');
		    $('#category').val('{{ $updateSelection->category }}');
		    @if ($updateSelection->requiresApproval == 1)
		    	$('#approvalRequirement').attr('checked', 'checked');
		    @endif
		    @if ($updateSelection->holdable == 1)
		    	$('#holdable').attr('checked', 'checked');
		    	$('#holdable').trigger('change');
		    	var days = 0;
		    	var upDays = Math.floor({{ $updateSelection->hold_time }});
		    	var min = '{{ $updateSelection->hold_time }}';
		    	min = min.split('.');
				if(min[1] != null){
					min = 30
				}else{
					min = '00';
				}
		    	while(upDays > 24){
		    		days++;
		    		upDays -= 24;
		    	}
		    	if(upDays.toString().length == 1){
		    		upDays = '0'+ upDays.toString();
		    	}
		    	$('#hold_day').val(days);
		    	$('#hold_time').val(upDays + ':' + min + ':00' );
		    @endif

		    @if ($updateSelection->target_sla != null)
			    var days = 0;
		    	var upDays = Math.floor({{ $updateSelection->target_sla }});
		    	var min = '{{ $updateSelection->target_sla }}';
		    	min = min.split('.');
				if(min[1] != null){
					min = 30
				}else{
					min = '00';
				}
		    	while(upDays > 24){
		    		days++;
		    		upDays -= 24;
		    	}
		    	if(upDays.toString().length == 1){
		    		upDays = '0'+ upDays.toString();
		    	}
		    	$('#sla_day').val(days);
		    	$('#sla_time').val(upDays + ':' + min + ':00' );
		    @endif
		@endisset

		$(document).on('click', '[name="cancel"]', function(e){
			e.preventDefault();
			swal({
				type: 'question',
				text: 'Are you sure you want to discard?',
				showConfirmButton: true,
				showCancelButton: true,
				confirmButtonText: 'Yes',
				cancelButtonText: 'No',
				showLoaderOnConfirm: true
			}).then(function(){
				window.location.href = '@if(\Gate::allows('it-staff')) {{ action('ItSupportTicketController@index') }} @elseif(\Gate::allows('approver')) {{ action('TicketController@index') }} @else {{ action('TicketController@index') }} @endif';
			});
		})
	</script>
@stop