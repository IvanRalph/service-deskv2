<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\WithoutApprovalQueueable;
use App\Jobs\WithApprovalQueueable;
use App\Jobs\TicketReopenedQueueable;
use App\Jobs\TicketDeletedQueueable;
use App\Ticket;
use App\RequestType;
use App\User;
use App\SurveyToken;
use App\RequestStatus;
use App\Email;
use App\Notification;
use Carbon\Carbon;


class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \Gate::denies('access-matrix', 14) ? redirect('/') : view('pages.tickets')->with('requestorTicket', true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \Gate::denies('access-matrix', 18) ? redirect('/') : view('pages.createTicket');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(\Gate::denies('access-matrix', 18)){
            return redirect('/');
        }
        
        $validator = \Validator::make($request->all(), [
            // 'type'=>'required|exists:request_type,type',
            // 'category'=>'required|exists:request_type,category',
            // 'priority_level'=>'required|exists:priority_levels,id',
            'description'=>'required|max:500',
            // 'supervisor'=>'nullable|exists:mysql2.users,id'
        ]);

        if($validator->fails()){
            $request->flash();
            return redirect('tickets/create')
                    ->withErrors($validator);
        }

        // $req = RequestType::where('type', $request->input('type'))->where('category', $request->input('category'))->first();

        $tic = Ticket::orderBy('id', 'DESC')->first();

        if(!$tic){
            $tic = new \stdClass();
            $tic->id = 1;
        }else{
            $tic->id += 1;
        }

        $pre = '';
        for($i = 4; $i > strlen($tic->id); $i--){
            $pre .= '0';
        }

        // if($req->requiresApproval == 1){
        //     $ticket = Ticket::create([
        //         'ticket_no'=>'ITSD-' . $pre . $tic->id,
        //         'request_type'=>$request->input('type'),
        //         'description'=>$request->input('description'),
        //         'priority_level'=>$request->input('priority_level'),
        //         'status'=>11,
        //         'assigned_to'=>$request->input('supervisor'),
        //         'requested_by'=>\Auth::user()->id
        //     ]);

        //     SurveyToken::create([
        //         'request_id'=>$ticket->id,
        //         'token'=>md5(rand(0,99999))
        //     ]);

        //     $ticket = Ticket::find($ticket->id);

        //     Notification::create([
        //         'request_id'=>$ticket->id,
        //         'notification'=>'<a href="'. action('ApprovalController@show', $ticket->id) .'" title="'. $ticket->ticket_no .' is pending your approval"><i class="fa fa-check-circle-o text-orange"></i> '. $ticket->ticket_no .' is pending your approval<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
        //         'user_id'=>$request->input('supervisor')
        //     ]);

        //     if($ticket){
        //         $to = \Auth::user()->email;

        //         $content = [
        //             'ticket_no'=>$ticket->ticket_no,
        //             'requestor'=>\Auth::user()->getDetails()->firstname . ' ' . \Auth::user()->getDetails()->lastname,
        //             'to'=>$to,
        //             'approver'=>User::where('id', $ticket->assigned_to)->first()->getDetails()->firstname . ' ' . User::where('id', $ticket->assigned_to)->first()->getDetails()->lastname,
        //             'subject'=>'For Approval - IT Support Ticket No. ' . $ticket->ticket_no
        //         ];

        //         dispatch(new WithApprovalQueueable($content))->delay(Carbon::now()->addSeconds(5));
        //     }
        // }else{
        //     $ticket = Ticket::create([
        //         'ticket_no'=>'ITSD-' . $pre . $tic->id,
        //         'request_type'=>$req->id,
        //         'description'=>$request->input('description'),
        //         'priority_level'=>$request->input('priority_level'),
        //         'status'=>5,
        //         'requested_by'=>\Auth::user()->id
        //     ]);

        //     SurveyToken::create([
        //         'request_id'=>$ticket->id,
        //         'token'=>md5(rand(0,99999))
        //     ]);

        //     if($ticket){
        //         $to = \Auth::user()->email;

        //         $content = [
        //             'ticket_no'=>$ticket->ticket_no,
        //             'requestor'=>\Auth::user()->fullName(),
        //             'to'=>$to,
        //             'subject'=>str_replace('%ticket_no%', $ticket->ticket_no, Email::where('request_status', 5)->first()->subject),
        //             'body'=>str_replace(['%requestor_name%', '%ticket_no%'], [\Auth::user()->fullName(), $ticket->ticket_no], Email::where('request_status', 5)->first()->body)
        //         ];

        //         dispatch(new WithoutApprovalQueueable($content))->delay(Carbon::now()->addSeconds(5));
        //     }
        // }

        $ticket = Ticket::create([
            'ticket_no'=>'ITSD-' . $pre . $tic->id,
            'description'=>$request->input('description'),
            'status'=>5,
            'requested_by'=>\Auth::user()->id
        ]);

        SurveyToken::create([
            'request_id'=>$ticket->id,
            'token'=>md5(rand(0,99999))
        ]);

        if($ticket){
            $to = \Auth::user()->email;

            $content = [
                'id'=>$ticket->id,
                'ticket_no'=>$ticket->ticket_no,
                'to'=>$to,
                'cc'=>'ralph.vitto@solarphilippines.ph',
                'subject'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 5)->first()->subject),
                'body'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 5)->first()->body)
            ];

            foreach (User::itStaff() as $staff) {
                Notification::create([
                    'request_id'=>$ticket->id,
                    'notification'=>'<a href="'. action('ItSupportTicketController@index') .'" title="'. $ticket->ticket_no .' is open and pending action."><i class="fa fa-exclamation-circle text-orange"></i>'. $ticket->ticket_no .' is open and pending action.<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
                    'user_id'=>$staff->id
                ]);
            }
            
            dispatch(new WithoutApprovalQueueable($content));
        }

        return redirect(action('TicketController@show', $ticket->id))->with('ticketCreated', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return \Gate::denies('access-matrix', 15) ? redirect('/') : view('pages.createTicket')->with('showTicket', Ticket::findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return \Gate::denies('access-matrix', 16) ? redirect('/') : view('pages.createTicket')->with('updateTicket', Ticket::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(\Gate::denies('access-matrix', 19)){
            return ['title'=>'Oops..', 'msg'=>'You do not have access to this module. Contact it.enterprise@solarphilippines.ph if you think otherwise.', 'type'=>'warning'];
        }

        if($request->input('reopen')){
            $ticket = Ticket::find($id);
            if($ticket->status != 2){
                return ['title'=>'Error', 'msg'=>'There was an error with your request. You can only reopen tickets that are already resolved.', 'type'=>'error'];
            }

            if($ticket->requestStatusTracker->first()->old_status == 11){
                $approver = $ticket->requestStatusTracker->where('new_status', 7)->first()->updated_by;
                Ticket::find($id)->update([
                    'status'=>11,
                    'assigned_to'=>$approver,
                    'remarks'=>$request->input('remarks')
                ]);

                $ticket = Ticket::find($id);

                $to = $ticket->userChild->email;
                $cc = 'ralph.vitto@solarphilippines.ph';

                $content = [
                    'ticket_no'=>$ticket->ticket_no,
                    'requestor'=>User::find($ticket->requested_by)->fullName(),
                    'to'=>$to,
                    'cc'=>$cc,
                    'subject'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 9)->first()->subject),
                    'body'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 9)->first()->body)
                ];

                dispatch(new TicketReopenedQueueable($content));

                return ['title'=>'Reopened', 'msg'=>'Ticket ID ' . $ticket->ticket_no . ' has been reopened and pending re-approval of your supervisor.', 'type'=>'success'];
            }else{
                Ticket::find($id)->update([
                    'status'=>9,
                    'assigned_to'=>null,
                    'remarks'=>$request->input('remarks')
                ]);

                $ticket = Ticket::find($id);

                $to = $ticket->userChild->email;
                $cc = 'ralph.vitto@solarphilippines.ph';

                $content = [
                    'ticket_no'=>$ticket->ticket_no,
                    'requestor'=>User::find($ticket->requested_by)->fullName(),
                    'to'=>$to,
                    'cc'=>$cc,
                    'subject'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 9)->first()->subject),
                    'body'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 9)->first()->body)
                ];

                dispatch(new TicketReopenedQueueable($content));

                return ['title'=>'Reopened', 'msg'=>'Ticket ID ' . $ticket->ticket_no . ' has been reopened.', 'type'=>'success'];
            }

        }

        if (\Gate::denies('access-matrix', 16)) {
            return redirect('/');
        }
        $validator = \Validator::make($request->all(), [
            'priority_level'=>'required|exists:priority_levels,id',
            'description'=>'required|max:500'
        ]);

        if($validator->fails()){
            $request->flash();
            return redirect(action('TicketController@edit', $id))
                    ->withErrors($validator);
        }

        $ticket = Ticket::where('id', $id)->update([
            'priority_level'=>$request->input('priority_level'),
            'description'=>$request->input('description'),
            'updated_by'=>\Auth::user()->id
        ]);

        return redirect(action('TicketController@edit', $id))->with('ticketUpdated', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (\Gate::denies('access-matrix', 17)) {
            return ['title'=>'Oops..', 'msg'=>'You do not have access to this module. Contact it.enterprise@solarphilippines.ph if you think otherwise.', 'type'=>'warning'];
        }
        $cc = Ticket::find($id)->status == 11 || Ticket::find($id)->status == 12 ? User::find(Ticket::find($id)->assigned_to)->email : '';

        $ticket = Ticket::find($id)->update([
            'status'=>3,
            'updated_by'=>\Auth::user()->id
        ]);

        if($ticket){
            $ticket = Ticket::find($id);

            $to = \Auth::user()->email;

            $content = [
                'ticket_no'=>$ticket->ticket_no,
                'requestor'=>\Auth::user()->fullName(),
                'to'=>$to,
                'cc'=>$cc,
                'subject'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 3)->first()->subject),
                'body'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 3)->first()->body)
            ];

            dispatch(new TicketDeletedQueueable($content));
        }

        return ['title'=>'Deleted', 'msg'=>'Ticket deleted successfully.', 'type'=>'success'];
    }
}
