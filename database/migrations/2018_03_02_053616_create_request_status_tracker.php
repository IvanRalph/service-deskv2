<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestStatusTracker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests_status_tracker', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request')->unsigned();
            $table->integer('old_status')->unsigned();
            $table->integer('new_status')->unsigned();
            $table->longText('remarks')->nullable();
            $table->integer('updated_by')->unsigned();
            $table->timestamp('updated_at');

            $table->foreign('request')
            ->references('id')
            ->on('requests')
            ->onUpdate('cascade');

            $table->foreign('old_status')
            ->references('id')
            ->on('status')
            ->onUpdate('cascade');

            $table->foreign('new_status')
            ->references('id')
            ->on('status')
            ->onUpdate('cascade');

            

            if (App::environment('production')) {
                $table->foreign('updated_by')
                ->references('id')
                ->on('tjsg_hris.employee_details')
                ->onUpdate('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests_status_tracker');
    }
}
