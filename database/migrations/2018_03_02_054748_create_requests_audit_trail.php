<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsAuditTrail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests_audit_trail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request')->unsigned();
            $table->string('field');
            $table->longText('old_value');
            $table->longText('new_value');
            $table->integer('action_by')->unsigned();
            $table->timestamp('action_date');

            $table->foreign('request')
            ->references('id')
            ->on('requests')
            ->onUpdate('cascade');

            

            if (App::environment('production')) {
                $table->foreign('action_by')
                ->references('id')
                ->on('tjsg_hris.employee_details')
                ->onUpdate('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests_audit_trail');
    }
}
