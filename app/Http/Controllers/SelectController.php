<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequestType;

class SelectController extends Controller
{
    public function getCategory(Request $request){
    	return RequestType::where([
            'type'=>$request->id,
            'status'=>1
        ])->get();
    }

    public function getRequestType(Request $request){
    	return RequestType::where([
    		'type'=>$request->input('type'),
    		'category'=>$request->input('category')
    	])->first() && RequestType::where([
    		'type'=>$request->input('type'),
    		'category'=>$request->input('category')
    	])->first()->requiresApproval == 1 ? '1' : '0';
    }
}
