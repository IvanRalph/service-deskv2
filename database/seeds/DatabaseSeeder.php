<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PriorityLevelSeeder::class);
        $this->call(StatusSeeder::class);
        $this->call(UserTypesSeeder::class);
        $this->call(TypeSeeder::class);
        $this->call(RequestTypeSeeder::class);
        $this->call(AccessMatrixSeeder::class);
        $this->call(EmailSeeder::class);
        // $this->call(ResolutionSeeder::class);
    }
}
