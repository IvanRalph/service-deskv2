<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestTypeAuditTrail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_type_audit_trail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_type')->unsigned();
            $table->string('field');
            $table->string('old_value');
            $table->string('new_value');
            $table->integer('action_by')->unsigned();
            $table->timestamp('action_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_type_audit_trail');
    }
}
