<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestStatus extends Model
{
    protected $table = 'requests_status_tracker';
    public $primaryKey = 'id';

    public $timestamps = false;

    public function ticket(){
    	return $this->belongsTo('App\Requests', 'id');
    }

    public function user(){
    	return $this->belongsTo('App\User', 'id');
    }
}
