@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
    @stack('css')
    @yield('css')
    <style type="text/css">
        .rating {
            color: #fffa65;
          unicode-bidi: bidi-override;
          direction: rtl;
        }
        .rating > span {
          display: inline-block;
          position: relative;
          width: 1.1em;
        }
        .rating > span:hover:before,
        .rating > span:hover ~ span:before {
           content: "\2605";
           position: absolute;
        }

        .bg-ivan{
            background-color: #f1f1f1!important;
        }
    </style>
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ? [
    'boxed' => 'layout-boxed',
    'fixed' => 'fixed',
    'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
    <div class="wrapper">

        <!-- Main Header -->
        <header class="main-header">
            @if(config('adminlte.layout') == 'top-nav')
            <nav class="navbar navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="navbar-brand">
                            {!! config('adminlte.logo', '<b>Admin</b>LTE') !!}
                        </a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            @each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item')
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
            @else
            <!-- Logo -->
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini">{!! config('adminlte.logo_mini', '<b>A</b>LT') !!}</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</span>
            </a>
            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                </a>
                <div class="col-sm-4 col-sm-offset-4 col-xs-4 col-xs-offset-3">
                    <h4 class="visible-lg">{{ config('app.name') }}</h4>
                    <h5 class="visible-xs">{{ config('app.name') }}</h5>
                </div>
            @endif
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu" id="notif-nav">

                    <ul class="nav navbar-nav" >
                        <li class="dropdown notifications-menu" id="notif-bell">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-bell-o"></i>
                                {!! App\Notification::where(['user_id'=>\Auth::user()->id, 'isRead'=>0])->first() || App\Notification::where(['user_id'=>\Auth::user()->id, 'isRead'=>0])->count() > 0 ? '<span class="label label-danger">'.App\Notification::where(['user_id'=>\Auth::user()->id, 'isRead'=>0])->count().'</span>' : '' !!}
                            </a>
                            <ul class="dropdown-menu">
                                {!! App\Notification::where(['user_id'=>\Auth::user()->id, 'isRead'=>0])->first() || App\Notification::where(['user_id'=>\Auth::user()->id, 'isRead'=>0])->count() > 0 ? '<li class="header">You have '. App\Notification::where(['user_id'=>\Auth::user()->id, 'isRead'=>0])->count() .' new notification(s)</li>' : 'You have no new notification' !!}
                                
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        {{-- <li>
                                            <a href="" title=""><i class="fa fa-pause-circle-o text-black"></i> is pending your approval.<div class="pull-right"><small><i class="fa fa-clock-o"></i> 123213</small></div></a>
                                        </li> --}}
                                        @foreach (App\Notification::where('user_id', \Auth::user()->id)->orderBy('id', 'DESC')->limit(10)->get() as $notif)
                                            <li {!! $notif->isRead == '0' ? 'class="bg-ivan"' : '' !!}>
                                                {!! $notif->notification !!}
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        {{-- <li>
                            @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))
                                <a href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}">
                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                </a>
                            @else
                                <a href="#"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                >
                                    <i class="fa fa-fw fa-power-off"></i> {{ \Auth::user()->getDetails()->firstname. ' ' . \Auth::user()->getDetails()->lastname }}
                                </a>
                                <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                                    @if(config('adminlte.logout_method'))
                                        {{ method_field(config('adminlte.logout_method')) }}
                                    @endif
                                    {{ csrf_field() }}
                                </form>
                            @endif
                        </li> --}}
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                <i class="fa fa-fw fa-user-o"></i> {{ \Auth::user()->fullName() }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="/user/profile"><i class="fa fa-fw fa-user-o"></i> User Profile</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-fw fa-power-off"></i> Log out</a></li>
                                <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                                    @if(config('adminlte.logout_method'))
                                        {{ method_field(config('adminlte.logout_method')) }}
                                    @endif
                                    {{ csrf_field() }}
                                </form>
                            </ul>
                        </li>
                    </ul>
                </div>
                @if(config('adminlte.layout') == 'top-nav')
                </div>
                @endif
            </nav>
        </header>

        @if(config('adminlte.layout') != 'top-nav')
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- Sidebar Menu -->
                <ul class="sidebar-menu" data-widget="tree">
                    @each('adminlte::partials.menu-item', $adminlte->menu(), 'item')
                </ul>
                <!-- /.sidebar-menu -->
            </section>
            <!-- /.sidebar -->
        </aside>
        @endif

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @if(config('adminlte.layout') == 'top-nav')
            <div class="container">
            @endif

            <!-- Content Header (Page header) -->
            <section class="content-header">
                @yield('content_header')
            </section>

            <!-- Main content -->
            <section class="content">

                @yield('content')

            </section>
            <!-- /.content -->
            @if(config('adminlte.layout') == 'top-nav')
            </div>
            <!-- /.container -->
            @endif
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right">
                Copyright © 2018.  IT Department | v1.0
            </div>
            <span style="color: #ffffff!important">IT Service Desk | IT Enterprise Division</span>
        </footer>

    </div>
    <!-- ./wrapper -->
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        var current = {{ App\Notification::where('user_id', \Auth::user()->id)->where('isRead', 0)->count() }}

        $(document).on('hidden.bs.dropdown', '#notif-bell', function () {
            $("#notif-nav").load(location.href + " #notif-nav");
        })

        $(document).on('shown.bs.dropdown', '#notif-bell', function () {
            $.ajax({
                url: '{{ route('read-notification') }}',
                type: 'GET',
                success: function(data, status){
                    current = 0;
                }
            });
        })

        @if (session()->has('survey'))
            $(document).ready(function(){
                swal({
                    title: 'Satisfactory Survey',
                    showConfirmButton: false,
                    showCancelButton: false,
                    html: 'Please rate our IT Services<br><b>{{ session('survey')->ticket_no }}</b><br><div class="rating" style="font-size: 30px;">'+
                        '<span data-num="5">☆</span><span data-num="4">☆</span><span data-num="3">☆</span><span data-num="2">☆</span><span data-num="1">☆</span>'+
                        '</div>',
                    allowOutsideClick: false,
                    allowEscapeKey: false
                });
            })

            $(document).on('click', '.rating span', function(){
                var rate = $(this).data('num');
                var request = {{ session('survey')->id }}
                swal({
                    title: 'Please confirm',
                    text: 'Are you sure you want to rate the IT Staff that assisted you ' + rate + ' Star(s)?',
                    showConfirmButton: true,
                    showCancelButton: true,
                    cancelButtonText: 'No',
                    confirmButtonText: 'Yes',
                    allowOutsideClick: false,
                    allowEscapeKey: false
                }).then(function(){
                    $.ajax({
                        url: '/survey/rate',
                        type: 'POST',
                        data: { rate: rate, request: request, staff: '{{ session('survey')->itStaff->id }}' },
                        dataType: 'JSON',
                        success: function(data, status){
                            swal(data.title, data.msg, data.type).then(function(){
                                window.location.href = "/";
                            });
                        }
                    })
                }).catch(function(){
                    window.location.href = "{{ session('survey')->url }}";
                });
            });
        @endif

        setInterval(function(){
            $.ajax({
                url: '/realtime-notif',
                type: 'GET',
                success: function(data, status){
                    if(current < data){
                        $("#notif-nav").load(location.href + " #notif-nav");
                        table.ajax.reload(null, false);
                        current = data;
                    }
                }
            });
        }, 1000);
    </script>
    @stack('js')
    @yield('js')
@stop
