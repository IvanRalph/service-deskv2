@component('mail::message')
# {{ $content['subject'] }}

{{ $content['body'] }}


@component('mail::button', ['url' => config('app.url')])
Click Here
@endcomponent

Thanks,<br>
{{ config('app.name') }}

<hr>
<span style="font-size: 10px;">If you’re having trouble clicking the button above, you may copy and paste the URL below into your web browser: {{ config('app.url') }}
</span>
@endcomponent
