<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';
    public $primaryKey = 'id';

    public $timestamps = false;

    public $fillable = ['request_id', 'notification', 'isRead', 'user_id'];
}
