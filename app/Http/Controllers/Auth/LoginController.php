<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail; 
use App\Mail\SendPass;
use App\User;
use App\Employee;
use App\AccessMatrix;
use App\UserAccess;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
{
        $this->middleware('guest')->except('logout');
    }

    public function validate_login(Request $request){
        $this->validate($request, [
            'email'=>'required|exists:mysql2.ltxx_systemadmin.users,email|email'
        ]);

        $user = User::findByEmail($request->input('email'));

        $access = AccessMatrix::checkIfHasAccess($user->employee_id);

        if(!$access){
            \Log::notice('No access to system', [
                'email'=>$request->input('email'),
                'employee_id'=>$user->employee_id,
                'ip'=>session('ip')
            ]);
            return view('auth.login')->with('noAccess', true);
        }

        $user = User::findByEmail($request->input('email'));
        if($user->password == '' || $user->password == null){
            $this->sendPassToEmail($request->input('email'));
            \Log::notice('User has no password', [
                'email'=>$request->input('email'),
                'employee_id'=>$user->employee_id
            ]);
            return view('auth.login')->with('noPass', true);
        }
        $request->flashOnly(['email']);
        return view('auth.login')->with('emailValidated', true);
    }

    public function login(Request $request){
        $this->validate($request, [
            'email'=>'required|exists:mysql2.ltxx_systemadmin.users,email|email',
            'password'=>'required'
        ]);

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password'), 'status'=>'active'], true)){
            \Log::notice('User authenticated', [
                'email'=>$request->input('email'),
                'employee_id'=>Auth::user()->employee_id,
                'ip'=>session('ip')
            ]);
            return redirect('/')->with('user', User::where('email', $request->input('email'))->first());
        }else{
            if(User::findByEmail($request->input('email'))->status != 'Active'){
                 return view('auth.login')->with('userInactive', true);
            }
            $request->flashExcept('password');
            \Log::warning('Invalid Password', [
                'email'=>$request->input('email'),
                'ip'=>session('ip')
            ]);
            return view('auth.login')->with('invalidPass', true);
        }
    }

    public function sendPassToEmail($email){
        $num = rand(1, 1000);
        $hash = substr(md5($num), 0, 8);
        $content = [

            'title'=> 'IT Service Desk', 

            'email'=>$email,

            'pass'=>$hash
            ];

        Mail::to($email)->send(new SendPass($content));
        $user = User::where('email', $email)->update([
                'password'=>bcrypt($hash)
            ]);

    }
}
