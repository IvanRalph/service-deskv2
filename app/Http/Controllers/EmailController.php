<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Email;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.emails');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.createEmail');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.createEmail')->with('updateEmail', Email::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->input('activate')){
            $validator = \Validator::make($request->all(), [
                'status'=>'required|in:0,1',
            ]);

            if($validator->fails()){
                \Log::error('Validator error');
                return ['title'=>'Error', 'msg'=>'There was an error with your request.', 'type'=>'error'];
            }

            $email = Email::find($id)->update([
                'status'=>$request->input('status')
            ]);

            if($email){
                return ['title'=>'Updated', 'msg'=>'Template updated successsfully.', 'type'=>'success'];
            }
        }
        $validator = \Validator::make($request->all(), [
            'subject'=>'required',
            'body'=>'required|max:500'
        ]);

        if($validator->fails()){
            $request->flash();
            return redirect(url()->previous())
                    ->withErrors($validator);
        }

        $email = Email::find($id)->update([
            'subject'=>$request->input('subject'),
            'body'=>$request->input('body')
        ]);

        return view('pages.emails')->with('emailUpdated', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
