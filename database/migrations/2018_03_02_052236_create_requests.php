<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ticket_no');
            $table->integer('request_type')->nullable()->unsigned();
            $table->longText('description');
            $table->integer('priority_level')->nullable()->unsigned();
            $table->integer('status')->unsigned();
            $table->longText('remarks')->nullable();
            $table->integer('resolution_id')->unsigned()->nullable();
            $table->integer('requested_by')->unsigned();
            $table->integer('assigned_to')->unsigned()->nullable();
            $table->timestamp('created_at');
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('request_type')
            ->references('id')
            ->on('request_type')
            ->onUpdate('cascade');

            $table->foreign('priority_level')
            ->references('id')
            ->on('priority_levels')
            ->onUpdate('cascade');

            $table->foreign('resolution_id')
            ->references('id')
            ->on('resolution_codes')
            ->onUpdate('cascade');

            $table->foreign('status')
            ->references('id')
            ->on('status')
            ->onUpdate('cascade');            

            if (App::environment('production')) {
                $table->foreign('requested_by')
                ->references('id')
                ->on('tjsg_hris.employee_details')
                ->onUpdate('cascade');

                $table->foreign('assigned_to')
                ->references('id')
                ->on('tjsg_hris.employee_details')
                ->onUpdate('cascade');

                $table->foreign('updated_by')
                ->references('id')
                ->on('tjsg_hris.employee_details')
                ->onUpdate('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
