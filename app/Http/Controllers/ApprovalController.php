<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\TicketApprovedQueueable;
use App\Jobs\TicketDisapprovedQueueable;
use App\Jobs\WithApprovalQueueable;
use App\Ticket;
use App\User;
use App\Notification;
use App\Email;
use Carbon\Carbon;

class ApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \Gate::denies('access-matrix', 22) ? redirect('/') : view('pages.approvals')->with('approverApprovals', true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return \Gate::denies('access-matrix', 23) ? redirect('/') : view('pages.createTicket')->with('showTicket', Ticket::find($id))->with('approver', true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->input('approver')){
            if (\Gate::denies('access-matrix', 24)) {
                return ['title'=>'Oops..', 'msg'=>'You do not have access to this module. Contact it.enterprise@solarphilippines.ph if you think otherwise.', 'type'=>'warning'];
            }
            switch($request->input('status')){
                case 7: //Approved
                    if(\Auth::user()->getDetails()->employee_id == \Auth::user()->itHead()->supervisor){
                        $ticket = Ticket::where('id', $id)->update([
                            'status'=>7,
                            'updated_by'=>\Auth::user()->id,
                            'remarks'=>null,
                            'assigned_to'=>null
                        ]);

                        $ticket = Ticket::find($id);

                        $to = \Auth::user()->email;

                        $content = [
                            'ticket_no'=>$ticket->ticket_no,
                            'requestor'=>\Auth::user()->getDetails()->firstname . ' ' . \Auth::user()->getDetails()->lastname,
                            'to'=>$to,
                            'approver'=>$ticket->userChild->fullNameById($ticket->assigned_to),
                            'subject'=>'Approved - IT Support Ticket No. ' . $ticket->ticket_no
                        ];

                        Notification::create([
                            'request_id'=>$ticket->id,
                            'notification'=>'<a href="'. action('TicketController@show', $ticket->id) .'" title="Your ticket '. $ticket->ticket_no .' has been approved."><i class="fa fa-check-circle-o text-green"></i>Your ticket '. $ticket->ticket_no .' has been approved.<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
                            'user_id'=>$ticket->requested_by
                        ]);

                        foreach (User::itStaff() as $staff) {
                            Notification::create([
                                'request_id'=>$ticket->id,
                                'notification'=>'<a href="'. action('ItSupportTicketController@index') .'" title="'. $ticket->ticket_no .' is open and pending action."><i class="fa fa-exclamation-circle text-orange"></i>'. $ticket->ticket_no .' is open and pending action.<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
                                'user_id'=>$staff->id
                            ]);
                        }

                        dispatch(new TicketApprovedQueueable($content))->delay(Carbon::now()->addSeconds(5));

                        return ['title'=>'Success', 'msg'=>'Ticket approved successfully.', 'type'=>'success'];
                    }

                    $ticket = Ticket::find($id)->update([
                        'status'=>12,
                        'updated_by'=>\Auth::user()->id,
                        'remarks'=>'Pending IT Head\'s Approval',
                        'assigned_to'=>\Auth::user()->itHeadId()->id
                    ]);

                    $ticket = Ticket::find($id);

                    $to = \Auth::user()->email;

                    $content = [
                        'ticket_no'=>$ticket->ticket_no,
                        'requestor'=>\Auth::user()->getDetails()->firstname . ' ' . \Auth::user()->getDetails()->lastname,
                        'to'=>$to,
                        'approver'=>$ticket->userChild->fullNameById($ticket->assigned_to),
                        'subject'=>'Approved - IT Support Ticket No. ' . $ticket->ticket_no
                    ];

                    Notification::create([
                        'request_id'=>$ticket->id,
                        'notification'=>'<a href="'. action('TicketController@show', $ticket->id) .'" title="Your ticket '. $ticket->ticket_no .' has been approved by your supervisor."><i class="fa fa-check-circle-o text-green"></i>Your ticket '. $ticket->ticket_no .' has been approved by your supervisor.<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
                        'user_id'=>$ticket->requested_by
                    ]);

                    Notification::create([
                        'request_id'=>$ticket->id,
                        'notification'=>'<a href="'. action('ApprovalController@show', $ticket->id) .'" title="'. $ticket->ticket_no .' is pending your approval."><i class="fa fa-exclamation-circle text-orange"></i>'. $ticket->ticket_no .' is pending your approval.<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
                        'user_id'=>User::itHeadId()->id
                    ]);

                    $to = User::itHeadId()->email;
                    $cc = [
                        User::find($ticket->requested_by)->email,
                        'ralph.vitto@solarphilippines.ph'
                    ];

                    $content = [
                        'ticket_no'=>$ticket->ticket_no,
                        'requestor'=>User::find($ticket->requested_by)->fullName(),
                        'to'=>$to,
                        'cc'=>$cc,
                        'subject'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 11)->first()->subject),
                        'body'=>str_replace(['%ticket_no%', '%requestor_name%', '%approver_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : '', User::itHeadId()->fullName()], Email::where('request_status', 11)->first()->body)
                    ];

                    dispatch(new WithApprovalQueueable($content));

                    return ['title'=>'Success', 'msg'=>'Ticket approved successfully.', 'type'=>'success'];
                    break;
                case 8: //Disapproved
                    $cc = [
                        Ticket::find($id)->assignedTo->email,
                        'ralph.vitto@solarphilippines.ph'
                    ];

                    $ticket = Ticket::find($id)->update([
                        'status'=>8,
                        'remarks'=>$request->input('remarks'),
                        'updated_by'=>\Auth::user()->id,
                        'assigned_to'=>null
                    ]);

                    if($ticket){
                        $ticket = Ticket::find($id);

                        $to = Ticket::find($id)->userChild->email;

                        $content = [
                            'ticket_no'=>$ticket->ticket_no,
                            'requestor'=>User::find($ticket->requested_by)->fullName(),
                            'to'=>$to,
                            'cc'=>$cc,
                            'subject'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 8)->first()->subject),
                            'body'=>str_replace(['%ticket_no%', '%requestor_name%', '%approver_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : '', User::itHeadId()->fullName()], Email::where('request_status', 8)->first()->body)
                        ];

                        Notification::create([
                            'request_id'=>$ticket->id,
                            'notification'=>'<a href="'. action('TicketController@show', $ticket->id) .'" title="Your ticket '. $ticket->ticket_no .' is put on hold."><i class="fa fa-times-circle-o text-black"></i>Your ticket '. $ticket->ticket_no .' has been disapproved<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
                            'user_id'=>$ticket->requested_by
                        ]);

                        Notification::create([
                            'request_id'=>$ticket->id,
                            'notification'=>'<a href="'. action('TicketController@show', $ticket->id) .'" title="Your ticket '. $ticket->ticket_no .' has been disapproved."><i class="fa fa-times-circle-o text-red"></i>Your ticket '. $ticket->ticket_no .' has been disapproved.<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
                            'user_id'=>$ticket->requested_by
                        ]);

                        dispatch(new TicketDisapprovedQueueable($content));
                    }

                    return ['title'=>'Success', 'msg'=>'Ticket disapproved successfully.', 'type'=>'success'];
                    break;
                default:
                    \Log::error('Invalid Status', $request->input('status'));
                    return ['title'=>'Error', 'msg'=>'There was an error with your request.', 'type'=>'error'];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
