<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_type', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type')->unsigned();
            $table->string('category');
            $table->integer('status')->default(1);
            $table->integer('requiresApproval')->default(0);
            $table->integer('holdable')->default(0);
            $table->string('hold_time')->nullable();
            $table->integer('created_by')->unsigned();
            $table->string('target_sla')->nullable();

            $table->foreign('type')
            ->references('id')
            ->on('type')
            ->onUpdate('cascade');

            

            if (App::environment('production')) {
                $table->foreign('created_by')
                ->references('id')
                ->on('tjsg_hris.employee_details')
                ->onUpdate('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_type');
    }
}
