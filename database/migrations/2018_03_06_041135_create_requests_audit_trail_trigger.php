<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsAuditTrailTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
            CREATE TRIGGER requests_audit_trail_trigger AFTER UPDATE ON requests FOR EACH ROW
                BEGIN
                    IF NEW.description <> OLD.description THEN
                        INSERT INTO requests_audit_trail (request, field, old_value, new_value, action_by) VALUES (OLD.id, "description", OLD.description, NEW.description, NEW.updated_by);
                    END IF;

                    IF NEW.priority_level <> OLD.priority_level THEN
                        INSERT INTO requests_audit_trail (request, field, old_value, new_value, action_by) VALUES (OLD.id, "priority_level", OLD.priority_level, NEW.priority_level, NEW.updated_by);
                    END IF;

                    IF NEW.status <> OLD.status OR OLD.assigned_to <> NEW.assigned_to THEN
                        INSERT INTO requests_status_tracker (request, old_status, new_status, remarks, updated_by) VALUES (OLD.id, OLD.status, NEW.status, NEW.remarks, NEW.updated_by);
                        INSERT INTO requests_audit_trail (request, field, old_value, new_value, action_by) VALUES (OLD.id, "status", OLD.status, NEW.status, NEW.updated_by);
                    END IF;
                END;
            ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `requests_audit_trail_trigger`');
    }
}
