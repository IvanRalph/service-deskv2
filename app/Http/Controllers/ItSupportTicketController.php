<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\User;
use App\SurveyToken;
use App\RequestStatus;
use App\RequestType;
use App\Jobs\TicketAssignedToQueuable;
use App\Jobs\TicketHoldQueueable;
use App\Jobs\TicketResolvedQueueable;
use App\Jobs\TicketCancelledQueueable;
use App\Jobs\WithApprovalQueueable;
use App\Notification;
use App\Email;
use Carbon\Carbon;

class ItSupportTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \Gate::denies('access-matrix', 2) ? redirect('/') : view('pages.tickets')->with('itSupportTicket', true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return \Gate::denies('access-matrix', 3) ? redirect('/') : view('pages.createTicket')->with('showTicket', Ticket::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->input('assign')){
            if(\Gate::denies('access-matrix', 4)){
                return ['title'=>'Oops..', 'msg'=>'You do not have access to this module. Contact it.enterprise@solarphilippines.ph if you think otherwise.', 'type'=>'warning'];
            }

            if($request->input('staff') == Ticket::find($id)->assigned_to){
                return ['title'=>'Oops..', 'msg'=>'This ticket is already assigned to you.', 'type'=>'warning'];
            }

            $ticket = Ticket::find($id)->update([
                'status'=>13,
                'remarks'=>'Assigned to ' . User::find($request->input('staff'))->fullName(),
                'assigned_to'=>$request->input('staff'),
                'updated_by'=>\Auth::user()->id
            ]);

            if($ticket){
                $ticket = Ticket::find($id);

                $to = $ticket->userChild->email;
                $cc = 'ralph.vitto@solarphilippines.ph';

                $content = [
                    'ticket_no'=>$ticket->ticket_no,
                    'requestor'=>User::find($ticket->requested_by)->fullName(),
                    'to'=>$to,
                    'cc'=>$cc,
                    'subject'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 13)->first()->subject),
                    'body'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 13)->first()->body)
                ];

                //notification for requestor
                Notification::create([
                    'request_id'=>$ticket->id,
                    'notification'=>'<a href="'. action('TicketController@show', $ticket->id) .'" title="Your ticket '. $ticket->ticket_no .' is assigned to an IT Staff."><i class="fa fa-user-plus text-blue"></i>Your ticket '. $ticket->ticket_no .' is assigned to an IT Staff.<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
                    'user_id'=>$ticket->requested_by
                ]);

                //notification for it staff assigned
                Notification::create([
                    'request_id'=>$ticket->id,
                    'notification'=>'<a href="'. action('ItSupportTicketController@show', $ticket->id) .'" title="'. $ticket->ticket_no .' has been assigned to you."><i class="fa fa-user-plus text-blue"></i>'. $ticket->ticket_no .' has been assigned to you.<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
                    'user_id'=>$ticket->assigned_to
                ]);

                dispatch(new TicketAssignedToQueuable($content));

                return ['title'=>'Success', 'msg'=>'Ticket assigned to '. User::find($request->input('staff'))->fullName(), 'type'=>'success'];
            }
        }

        if($request->input('hold')){
            if(\Gate::denies('access-matrix', 7)){
                return ['title'=>'Oops..', 'msg'=>'You do not have access to this module. Contact it.enterprise@solarphilippines.ph if you think otherwise.', 'type'=>'warning'];
            }

            $ticket = Ticket::where('id', $id)->update([
                'status'=>$request->input('status'),
                'remarks'=>$request->input('remarks'),
                'updated_by'=>\Auth::user()->id
            ]);

            if($ticket){
                $ticket = Ticket::find($id);

                if($request->input('status') == 1){
                    $to = $ticket->userChild->email;
                    $cc = 'ralph.vitto@solarphilippines.ph';

                    $content = [
                        'ticket_no'=>$ticket->ticket_no,
                        'requestor'=>User::find($ticket->requested_by)->fullName(),
                        'to'=>$to,
                        'cc'=>$cc,
                        'subject'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 1)->first()->subject),
                        'body'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 1)->first()->body)
                    ];

                    Notification::create([
                        'request_id'=>$ticket->id,
                        'notification'=>'<a href="'. action('TicketController@show', $ticket->id) .'" title="Your ticket '. $ticket->ticket_no .' is put on hold."><i class="fa fa-pause-circle-o text-black"></i>Your ticket '. $ticket->ticket_no .' is put on hold.<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
                        'user_id'=>$ticket->requested_by
                    ]);

                    dispatch(new TicketHoldQueueable($content));
                    return ['title'=>'Success', 'msg'=>'Ticket has been put on hold', 'type'=>'success'];
                }else{
                    Notification::create([
                        'request_id'=>$ticket->id,
                        'notification'=>'<a href="'. action('TicketController@show', $ticket->id) .'" title="Your ticket '. $ticket->ticket_no .' has been resumed."><i class="fa fa-play-circle-o text-black"></i>Your ticket '. $ticket->ticket_no .' has been resumed.<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
                        'user_id'=>$ticket->requested_by
                    ]);

                    return ['title'=>'Success', 'msg'=>'Ticket has been resumed', 'type'=>'success'];
                }
            }
        }

        if($request->input('resolve')){
            if(\Gate::denies('access-matrix', 5)){
                return ['title'=>'Oops..', 'msg'=>'You do not have access to this module. Contact it.enterprise@solarphilippines.ph if you think otherwise.', 'type'=>'warning'];
            }

            $ticket = Ticket::find($id)->update([
                'status'=>2,
                'resolution_id'=>$request->input('resolution_code'),
                'remarks'=>$request->input('remarks'),
                'updated_by'=>\Auth::user()->id
            ]);

            if($ticket){
                $ticket = Ticket::find($id);

                $to = $ticket->userChild->email;
                $cc = 'ralph.vitto@solarphilippines.ph';

                $content = [
                    'ticket_no'=>$ticket->ticket_no,
                    'requestor'=>User::find($ticket->requested_by)->fullName(),
                    'to'=>$to,
                    'cc'=>$cc,
                    'subject'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 2)->first()->subject),
                    'body'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 2)->first()->body),
                    'survey_token'=>$ticket->survey->token,
                    'ticket_id'=>$ticket->id
                ];

                Notification::create([
                    'request_id'=>$ticket->id,
                    'notification'=>'<a href="'. action('TicketController@show', $ticket->id) .'" title="Your ticket '. $ticket->ticket_no .' has been resolved."><i class="fa fa-check-circle-o text-green"></i>Your ticket '. $ticket->ticket_no .' has been resolved.<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
                    'user_id'=>$ticket->requested_by
                ]);


                dispatch(new TicketResolvedQueueable($content));
                return ['title'=>'Success', 'msg'=>'Ticket has been resolved', 'type'=>'success'];
            }
        }

        if($request->input('cancel')){
            if(\Gate::denies('access-matrix', 6)){
                return ['title'=>'Oops..', 'msg'=>'You do not have access to this module. Contact it.enterprise@solarphilippines.ph if you think otherwise.', 'type'=>'warning'];
            }

            $ticket = Ticket::where('id', $id)->update([
                'status'=>4,
                'remarks'=>$request->input('remarks'),
                'updated_by'=>\Auth::user()->id
            ]);

            if($ticket){
                $ticket = Ticket::find($id);

                $to = $ticket->userChild->email;
                $cc = 'ralph.vitto@solarphilippines.ph';

                $content = [
                    'ticket_no'=>$ticket->ticket_no,
                    'requestor'=>User::find($ticket->requested_by)->fullName(),
                    'to'=>$to,
                    'cc'=>$cc,
                    'subject'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 4)->first()->subject),
                    'body'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 4)->first()->body)
                ];

                dispatch(new TicketCancelledQueueable($content));
                return ['title'=>'Success', 'msg'=>'Ticket has been cancelled', 'type'=>'success'];
            }
        }

        $validator = \Validator::make($request->all(), [
            'type'=>'required|exists:request_type,type',
            'category'=>'required|exists:request_type,category',
            'priority_level'=>'required|exists:priority_levels,id',
            'supervisor'=>'nullable|exists:mysql2.users,id'
        ]);

        if($validator->fails()){
            $request->flash();
            return redirect(action('ItSupportTicketController@show', $id))
                    ->withErrors($validator);
        }

        $req = RequestType::where('type', $request->input('type'))->where('category', $request->input('category'))->first();

        if($req->requiresApproval == 1){
            $validator = \Validator::make($request->all(), [
                'supervisor'=>'required|exists:mysql2.users,id'
            ]);

            if($validator->fails()){
                $request->flash();
                return redirect(action('ItSupportTicketController@show', $id))
                        ->withErrors($validator);
            }

            $ticket = Ticket::find($id)->update([
                'status'=>11,
                'request_type'=>$req->id,
                'priority_level'=>$request->input('priority_level'),
                'remarks'=>'',
                'assigned_to'=>$request->input('supervisor')
            ]);

            $ticket = Ticket::find($id);

            if($ticket){
                $to = User::find($request->input('supervisor'))->email;
                $cc = [
                    User::find($ticket->requested_by)->email,
                    'ralph.vitto@solarphilippines.ph'
                ];

                $content = [
                    'ticket_no'=>$ticket->ticket_no,
                    'requestor'=>User::find($ticket->requested_by)->fullName(),
                    'to'=>$to,
                    'cc'=>$cc,
                    'subject'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 11)->first()->subject),
                    'body'=>str_replace(['%ticket_no%', '%requestor_name%', '%approver_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : '', User::find($request->input('supervisor'))->fullName()], Email::where('request_status', 11)->first()->body)
                ];

                Notification::create([
                    'request_id'=>$ticket->id,
                    'notification'=>'<a href="'. action('ApprovalController@show', $ticket->id) .'" title="'. $ticket->ticket_no .' is pending your approval"><i class="fa fa-check-circle-o text-orange"></i> '. $ticket->ticket_no .' is pending your approval<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
                    'user_id'=>$request->input('supervisor')
                ]);

                Notification::create([
                    'request_id'=>$ticket->id,
                    'notification'=>'<a href="'. action('TicketController@show', $ticket->id) .'" title="Your ticket '. $ticket->ticket_no .' is now pending approval."><i class="fa fa-check-circle-o text-orange"></i>Your ticket '. $ticket->ticket_no .' is now pending approval.<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
                    'user_id'=>$ticket->requested_by
                ]);

                dispatch(new WithApprovalQueueable($content));
            }
        }else{
            Ticket::find($id)->update([
                'request_type'=>$req->id,
                'priority_level'=>$request->input('priority_level')
            ]);
        }

        return redirect(action('ItSupportTicketController@show', $id))->with('ticketUpdated', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
