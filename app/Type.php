<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'type';
    public $primaryKey = 'id';

    public function requestType(){
    	return $this->belongsTo('App\RequestType', 'type');
    }

    public function categoryChild(){
    	return $this->hasMany('App\Category', 'id');
    }
}
