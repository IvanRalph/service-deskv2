<?php

use Illuminate\Database\Seeder;

class RequestTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('request_type')->insert([
        	[
        		'type'=>1,
                'category'=>'Internet',
                'requiresApproval'=>0,
        		'created_by'=>1
        	],
            [
                'type'=>2,
                'category'=>'Laptop',
                'requiresApproval'=>1,
                'created_by'=>1
            ]
        ]);
    }
}
