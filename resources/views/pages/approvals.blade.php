@extends('adminlte::page')

@section('title', 'Approvals | IT Service Desk')

@section('content_header')
    <h1>Ticket Approvals</h1>
@stop

@section('content')
	<div class='notifications top-right'></div>
	<table id="approverTable" class="table table-striped table-hover table-bordered text-center">
		<thead>
			<tr>
				<th>Ticket No.</th>
				<th>Type</th>
				<th>Category</th>
				<th>Description</th>
				<th>Status</th>
				<th>Created By</th>
				<th>Assigned To</th>
				<th>Action</th>
			</tr>
		</thead>	
	</table>
@stop

@section('js')
	{{-- ================================A P P R O V E R================================ --}}
	@isset ($approverApprovals)
	    <script type="text/javascript">
	    	var table = $('#approverTable').DataTable({
	    		processing: true,
	    		'order': [],
	    		ajax: '/datatable/approvals',
	    		columns: [
	    			{ data: 'ticket_no', width: '80px' },
	    			{ data: 'type', width: '50%' },
	    			{ data: 'category', width: '50%' },
	    			{ data: 'description', width: '150px' },
	    			{ data: 'status', width: '100px' },
	    			{ data: 'created_by', width: '100px' },
	    			{ data: 'assigned_to', width: '100px' },
	    			{ data: null, width: '200px', searchable: false, sortable: false }
	    		],
	    		dom: 'r<"pull-right"B><"pull-left"l {{ \Gate::allows('access-matrix', 26) ? 'f' : '' }} >tip',
	    		lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
	    		pageLength: 10,
	    		columnDefs: [
	    		{
	    			targets: -1,
	    			render: function(a, b, data, d){
	    				var btn = '';
	    				@if (\Gate::allows('access-matrix', 24))
	    				if((data.status_id == 11 || data.status_id == 12) && data.assigned_id == {{ \Auth::user()->id }}){
	    					btn += "<a href='#' data-id='"+ data.id +"' style='font-size: 10px;' data-status='7' class='btn btn-success approverBtn'>Approve</a>";
	    					btn += "<a href='#' data-id='"+ data.id +"' style='font-size: 10px;' data-status='8' class='btn btn-danger approverBtn'>Disapprove</a>";
	    				}
	    				@endif
	    				return btn;
	    			}
	    		},
	    		{
					targets: [4],
					render: function(data, type, row){
						if(data == null){
							return 'Pending';
						}
						var color = "";
						switch(row.status_id){
							case '1':
								color = 'gray';
								break;
							case '2':
								color = 'primary';
								break;
							case '4': 
								color = 'red';
								break;
							case '5':
								color = 'green';
								break;
							case '6':
								color = 'gray';
								break;
							case '7':
								color = 'green';
								break;
							case '8':
								color = 'danger';
								break;
							case '9':
								color = 'green';
								break;
							case '10':
								color = 'maroon';
								break;
							case '11':
							case '12':
								color = 'orange';
								break;
							case '13':
								color = 'navy';
								break;
							default:
								color = 'black';
								break;
						}


						return '<span class="label bg-'+ color +'">'+data+'</span>';
					}
				},
				{
					targets: [3],
					render: function(data, type, row){
						if(data != null){
							return data.length > 10 ?
							'<span data-toggle="tooltip" title="' + data + '">' + data.substr( 0, 10 ) + '...' + '</span>':
							data;
						}else{
							return data;
						}
					}
				},
	    		],
	    		buttons: [
	    		@if (\Gate::allows('access-matrix', 25))
	    		{
	    			extend: 'excel',
	    			text: 'Export',
	    			exportOptions: {
	    				columns: [0,1,2,3,4,5,6]
	    			}
	    		}
	    		@endif
	    		],
	    		"scrollX": true,
	    		"fixedHeader": true
	    	});

	    	@if (\Gate::allows('access-matrix', 23))
	    	$('#approverTable tbody').on( 'click', 'tr td:not(td:last-child)', function () {
	    		window.location.href = "/approvals/"+ table.row( this ).data().id;
	    	} );

	    	$('#approverTable tbody').on( 'mouseover', 'tr td:not(td:last-child)', function () {
	    		$(this).css( 'cursor', 'pointer' );
	    	} );
	    	@endif

	    	@if (\Gate::allows('access-matrix', 24))
	    	$(document).on('click', '.approverBtn', function(e){
	    		e.preventDefault();
	    		var id = $(this).data('id');
	    		var status_id = $(this).data('status');
	    		var type = "";
	    		var remarks = "";
	    		switch(status_id){
	    			case 7:
	    				type = "Approve";
	    				break;
	    			case 8:
	    				type = "Disapprove";
	    				break;
	    			default:
	    				swal('Error', 'Invalid Request, reloading page.', 'error').then(function(){
	    					window.location.reload();
	    				});
	    				break;
	    		}
	    		if(status_id == 8){
	    			console.log('8');
	    			swal({
	    				title: 'Disapproval reason',
	    				text: 'Enter reason for disapproval',
	    				input: 'textarea',
	    				showCancelButton: true,
	    				confirmButtonText: 'Proceed',
	    				type: 'question',
	    				showLoaderOnConfirm: true
	    			}).then(function(result){
	    				swal({
	    					text: 'Are you sure you want to '+ type +' this Ticket?',
	    					showCancelButton: true,
	    					cancelButtonText: 'No',
	    					confirmButtonText: 'Yes',
	    					type: 'question',
	    					showLoaderOnConfirm: true
	    				}).then(function(){
	    					$.ajax({
	    						url: '../approvals/' + id,
	    						type: 'PATCH',
	    						data: {approver: true, status: status_id, remarks: result},
	    						dataType: 'JSON',
	    						success: function(data, result){
	    							swal(data.title, data.msg, data.type).then(function(){
	    								table.ajax.reload();
	    								$("#notif-nav").load(location.href + " #notif-nav");
	    							});
	    						}
	    					});
	    				});
	    			});
	    		}else{
	    			swal({
	    				text: 'Are you sure you want to '+ type +' this Ticket?',
	    				showCancelButton: true,
	    				cancelButtonText: 'No',
	    				confirmButtonText: 'Yes',
	    				type: 'question',
	    				showLoaderOnConfirm: true
	    			}).then(function(){
	    				$.ajax({
	    					url: '../approvals/' + id,
	    					type: 'PATCH',
	    					data: {approver: true, status: status_id},
	    					dataType: 'JSON',
	    					success: function(data, result){
	    						swal(data.title, data.msg, data.type).then(function(){
	    							table.ajax.reload();
	    							$("#notif-nav").load(location.href + " #notif-nav");
	    						});
	    					}
	    				});
	    			});
	    		}
	    	})

	    	@endif
	    	{{-- =============================E N D  O F  A P P R O V E R============================= --}}
	    </script>

	@else
	{{-- =============================I T  S T A F F  A P P R O V E R============================= --}}
		<script type="text/javascript">
			var table = $('#approverTable').DataTable({
				processing: true,
				'order': [],
				ajax: '{{ route('it-staff-datatable-approver') }}',
				columns: [
					{ data: 'ticket_no', width: '80px' },
					{ data: 'type', width: '50%' },
					{ data: 'category', width: '50%' },
					{ data: 'description', width: '150px' },
					{ data: 'status', width: '100px' },
					{ data: 'created_by', width: '100px' },
					{ data: 'assigned_to', width: '100px' },
					{ data: null, width: '200px', searchable: false, sortable: false }
				],
				dom: 'r<"pull-right"B><"pull-left"l {{ \Gate::allows('access-matrix', 13) ? 'f' : '' }} >tip',
				lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
				pageLength: 10,
				columnDefs: [
				{
					targets: -1,
					render: function(a, b, data, d){
						var btn = '';
						@if (\Gate::allows('access-matrix', 11))
						btn = "<a href='/it-staff/approvals/"+ data.id +"' style='font-size: 10px;' class='btn btn-default'>View</a>";
						@endif
						return btn;
					}
				},
				{
					targets: [4],
					render: function(data, type, row){
						if(data == null){
							return 'Pending';
						}
						var color = "";
						switch(row.status_id){
							case '1':
								color = 'gray';
								break;
							case '2':
								color = 'primary';
								break;
							case '4': 
								color = 'red';
								break;
							case '5':
								color = 'green';
								break;
							case '6':
								color = 'gray';
								break;
							case '7':
								color = 'green';
								break;
							case '8':
								color = 'danger';
								break;
							case '9':
								color = 'green';
								break;
							case '10':
								color = 'maroon';
								break;
							case '11':
							case '12':
								color = 'orange';
								break;
							case '13':
								color = 'navy';
								break;
							default:
								color = 'black';
								break;
						}


						return '<span class="label bg-'+ color +'">'+data+'</span>';
					}
				},
				{
					targets: [3],
					render: function(data, type, row){
						if(data != null){
							return data.length > 10 ?
							'<span data-toggle="tooltip" title="' + data + '">' + data.substr( 0, 10 ) + '...' + '</span>':
							data;
						}else{
							return data;
						}
					}
				},
				],
				buttons: [
				@if (\Gate::allows('access-matrix', 12))
				{
					extend: 'excel',
					text: 'Export',
					exportOptions: {
						columns: [0,1,2,3,4,5,6]
					}
				}
				@endif
				],
				"scrollX": true,
				"fixedHeader": true
			});

			@if (\Gate::allows('access-matrix', 11))
			$('#approverTable tbody').on( 'click', 'tr td:not(td:last-child)', function () {
				window.location.href = "/it-staff/approvals/"+ table.row( this ).data().id;
			} );

			$('#approverTable tbody').on( 'mouseover', 'tr td:not(td:last-child)', function () {
				$(this).css( 'cursor', 'pointer' );
			} );
			@endif
		</script>
		{{-- ========================E N D  O F  I T  S T A F F  A P P R O V E R======================== --}}
	@endisset
@stop