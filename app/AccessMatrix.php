<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessMatrix extends Model
{
    protected $table = 'access_matrix_department';
    public $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'department_id', 'system_id', 'hasAccess'
    ];

    protected $connection = 'mysql2';

    public static function checkIfHasAccess($emp){
    	$builder = AccessMatrix::join('tjsg_hris.department_details AS dd', 'ltxx_systemadmin.access_matrix_department.department_id', '=', 'dd.department_id')
        ->join('tjsg_hris.employee_details as ed', 'dd.department_id', '=', 'ed.department_id')
        ->join('ltxx_systemadmin.system_details AS sd', 'access_matrix_department.system_id', '=', 'sd.id')
        ->where([
            'access_matrix_department.system_id'=>'2',
            'access_matrix_department.hasAccess'=>"1",
            'access_matrix_department.department_id'=>\DB::raw('ed.department_id'),
            'ed.employee_id'=>$emp
        ])->first();

        return $builder ? true : false;
    }
}
