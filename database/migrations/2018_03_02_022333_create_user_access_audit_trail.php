<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAccessAuditTrail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_access_audit_trail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_access')->unsigned();
            $table->integer('old_type')->unsigned();
            $table->integer('new_type')->unsigned();

            $table->foreign('user_access')
            ->references('id')
            ->on('user_access')
            ->onUpdate('cascade');

            $table->foreign('old_type')
            ->references('id')
            ->on('user_types')
            ->onUpdate('cascade');

            $table->foreign('new_type')
            ->references('id')
            ->on('user_types')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_access_audit_trail');
    }
}
