<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class RequestType extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $table = 'request_type';
    public $primaryKey = 'id';

    public $timestamps = false;

    protected $auditInclude = [
            'type',
            'category',
            'status',
            'requiresApproval',
            'holdable',
            'hold_time',
            'target_sla'
        ];

    protected $fillable = ['type', 'category', 'status', 'requiresApproval', 'holdable', 'hold_time', 'created_by', 'target_sla'];

    public function typeChild(){
    	return $this->hasMany('App\Type', 'id');
    }

    public function categoryChild(){
    	return $this->hasMany('App\Category', 'id');
    }

    public function ticket(){
    	return $this->belongsTo('App\Ticket', 'id');
    }
}
