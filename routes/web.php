<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware'=>['guest', 'revalidate']], function(){
	Route::get('/validate_login', function(){
		return redirect('/');
	});
	
	Route::post('/validate_login', ['as'=>'validate_login', 'uses'=>'Auth\LoginController@validate_login']);

	Route::post('/login', ['as'=>'login', 'uses'=>'Auth\LoginController@login']);
});

Route::group(['middleware'=>['auth', 'revalidate']], function(){
	Route::prefix('it-staff')->group(function(){
		Route::resource('tickets', 'ItSupportTicketController');

		Route::resource('approvals', 'ItSupportApprovalController');

		Route::get('datatable/tickets', 'DatatableController@itSupportTickets')->name('it-staff-datatable');

		Route::get('datatable/it-staff/approvals', 'DatatableController@itSupportApprovals')->name('it-staff-datatable-approver');
	});

	Route::prefix('setup')->group(function(){
		Route::resource('access-matrix', 'AccessMatrixController')->middleware('itstaff');

		Route::resource('requests-selection', 'RequestSelectionController')->middleware('itstaff');

		Route::resource('email-notifications', 'EmailController')->middleware('itstaff');

		Route::resource('resolution-codes', 'ResolutionController')->middleware('itstaff');

		Route::get('datatable/setup/requestSelection', 'DatatableController@requestSelection')->name('setup-request-selection')->middleware('itstaff');

		Route::get('datatable/setup/emailNotif', 'DatatableController@emailNotif')->name('setup-email-notif')->middleware('itstaff');

		Route::get('datatable/setup/resolutionCode', 'DatatableController@resolutionCode')->name('setup-resolution-code')->middleware('itstaff');
	});

	Route::post('survey/rate', 'SurveyController@store');

	Route::resource('tickets', 'TicketController');

	Route::get('datatable/tickets', 'DatatableController@tickets');

	Route::post('select/category', 'SelectController@getCategory')->name('select-category');

	Route::post('select/request-type', 'SelectController@getRequestType')->name('select-request-type');

	Route::get('survey/{id}/{token}', 'SurveyController@startSurvey');

	Route::resource('approvals', 'ApprovalController');

	Route::get('datatable/approvals', 'DatatableController@approvals');

	Route::resource('user/profile', 'ProfileController');

	Route::get('read-notification', 'HomeController@readNotification')->name('read-notification');

	Route::get('realtime-notif', function(){
		return App\Notification::where('user_id', \Auth::user()->id)->where('isRead', 0)->count();
	});
});