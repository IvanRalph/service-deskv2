<?php

use Illuminate\Database\Seeder;

class PriorityLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('priority_levels')->insert([
        	[
        		'level'=>'Low'
        	],
        	[
        		'level'=>'Normal'
        	],
        	[
        		'level'=>'High'
        	],
        	[
        		'level'=>'Urgent'
        	],
        	[
        		'level'=>'Immediate'
        	]
        ]);
    }
}
