<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\UserAccess;
use App\Employee;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements Auditable
{
    use \OwenIt\Auditing\Auditable; 
    use Notifiable;

    protected $table = 'users';
    public $primaryKey = 'id';
    public $timestamps = false;

    protected $auditInclude = [
            'email',
            'status',
            'isDeleted',
        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'employee_id', 'status', 'isDeleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $connection = 'mysql2';

    public function Employee(){
        return $this->belongsTo('App\Employee', 'employee_id');
    }
    
    public function isUserType(){
        $ticket = Ticket::where([
                'status'=>11,
                'assigned_to'=>$this->id
            ])->first();

        if($this->itHead()->supervisor == $this->employee_id){
            return 4;
        }else if($this->departmentDetails()->department_id == '20180024IT'){
            return 3;
        }else if($ticket){
            return 2;
        }else if($this->departmentDetails()->department_id != '20180024IT'){
            return 1;
        }
    }

    public function getDetails(){
        return Employee::where('employee_id', $this->employee_id)->first();
    }

    public function fullName(){
        $u = User::join('tjsg_hris.employee_details', 'users.employee_id', 'employee_details.employee_id')
        ->where('users.id', $this->id)
        ->select(\DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE employee_id = users.employee_id) AS full_name'))
        ->first();
        return $u ? $u->full_name : '';
    }

    public static function itHeadId(){
        return User::where('employee_id', \Auth::user()->itHead()->supervisor)->first();
    }

    public static function itHead(){
        $head = \DB::table('tjsg_hris.department_details')->where('department_id', '20180024IT')->first();
        return $head;
    }

    public static function itStaff(){
        return User::join('tjsg_hris.employee_details', 'users.employee_id', 'employee_details.employee_id')
        ->where('employee_details.department_id', '20180024IT')
        ->select('users.id', \DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE employee_id = users.employee_id) AS full_name'))
        ->get();
    }

    public function fullNameById($id){
        $u = User::join('tjsg_hris.employee_details', 'users.employee_id', 'employee_details.employee_id')
        ->where('users.id', $id)
        ->select(\DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE employee_id = users.employee_id) AS full_name'))
        ->first();
        return $u ? $u->full_name : '';
    }

    public static function findByEmail($email){
        $builder = User::where('email', $email)->first();

        return $builder ? $builder : false;
    }

    public function departmentDetails(){
        return Employee::join('department_details', 'employee_details.department_id', 'department_details.department_id')->where('employee_details.employee_id', $this->employee_id)->first();
    }
}
