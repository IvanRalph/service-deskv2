<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\TicketClosedQueueable;
use App\SurveyToken;
use App\Ticket;
use App\Survey;
use App\Email;
use App\User;
use App\Notification;
use Carbon\Carbon;

class SurveyController extends Controller
{
    public function startSurvey($request, $token){
    	if(!SurveyToken::where('request_id', $request)->first() || SurveyToken::where('request_id', $request)->first()->token != $token){
    		abort(403, 'Token is either expired or does not exist.');
    	}else{
    		$ticket = Ticket::find($request);
    		$ticket->url = url()->current();
    		return redirect('/')->with('survey', $ticket);
    	}
    }

    public function store(Request $request){
    	$validator = \Validator::make($request->all(), [
    	    'request'=>'required|exists:requests,id',
    	    'rate'=>'required|in:1,2,3,4,5',
    	    'staff'=>'required|exists:requests,assigned_to'
    	]);

    	$ticket = Ticket::where([
    		'id'=>$request->input('request'),
    		'assigned_to'=>$request->input('staff')
    	])->first();

    	if($validator->fails() || !$ticket){
    		\Log::error('Data has been tampered', $request->input());
    	   	return ['title'=>'Error', 'msg'=>'There was an error in submitting your request', 'type'=>'error'];
    	}

    	$survey = Survey::create([
    		'request'=>$request->input('request'),
    		'rate'=>$request->input('rate'),
    		'staff'=>$request->input('staff')
    	]);

    	Ticket::find($request->input('request'))->update([
    		'status'=>6,
            'remarks'=>'',
    		'updated_by'=>\Auth::user()->id
    	]);

        $ticket = Ticket::find($request->input('request'));

        $to = $ticket->userChild->email;
        $cc = 'ralph.vitto@solarphilippines.ph';

        $content = [
            'ticket_no'=>$ticket->ticket_no,
            'requestor'=>User::find($ticket->requested_by)->fullName(),
            'to'=>$to,
            'cc'=>$cc,
            'subject'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 6)->first()->subject),
            'body'=>str_replace(['%ticket_no%', '%requestor_name%'], [$ticket->ticket_no, isset($ticket->requested_by) ? User::find($ticket->requested_by)->fullName() : ''], Email::where('request_status', 6)->first()->body)
        ];

        foreach (User::itStaff() as $staff) {
            Notification::create([
                'request_id'=>$ticket->id,
                'notification'=>'<a href="'. action('ItSupportTicketController@show', $ticket->id) .'" title="'. $ticket->ticket_no .' has been closed."><i class="fa fa-star text-yellow"></i> '. $ticket->ticket_no .' has been closed.<div class="pull-right"><small><i class="fa fa-clock-o"></i> '. date('M j, Y g:i a', strtotime($ticket->created_at)) .'</small></div></a>',
                'user_id'=>$staff->id
            ]);
        }

        dispatch(new TicketClosedQueueable($content));

    	SurveyToken::where('request_id', $request->input('request'))->delete();
    	return ['title'=>'Thank you!', 'msg'=>'We appreciate your rating, this will be used to improve our service.', 'type'=>'success'];
    }
}
