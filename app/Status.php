<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';
    public $primaryKey = 'id';

    public $timestamps = false;

    public function ticket(){
    	return $this->hasMany('App\Ticket', 'id');
    }
}
