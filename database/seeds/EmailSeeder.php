<?php

use Illuminate\Database\Seeder;

class EmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('email_notification')->insert([
        	[
        		'subject'=>'[OPEN] IT Support Ticket No. %Ticket_No%',
                'request_status'=>5,
                'body'=>'Hi, %requestor_name%. Your request with Ticket No. %Ticket_No% has been created. An IT representative will assist you shortly.',
        	],
            [
                'subject'=>'[For Approval] IT Support Ticket No. %Ticket_No%',
                'request_status'=>11,
                'body'=>'Hi, %approver_name%. %requestor_name% have created a request with Ticket No. %Ticket_No% and is subject for your approval.',
            ],
            [
                'subject'=>'[REOPENED] IT Support Ticket No. %Ticket_No%',
                'request_status'=>9,
                'body'=>'Hi, %requestor_name%. Your request with TIcket No. %Ticket_No% has been reopened. An IT representative will assist you shortly.',
            ],
            [
                'subject'=>'[Re-Approval] IT Support Ticket No. %Ticket_No%',
                'request_status'=>11,
                'body'=>'Hi, %approver_name%. %requestor_name% \'s request with Ticket No. %ticket_No% has been reopened and is subject for your re-approval.',
            ],
            [
                'subject'=>'[RESOLVED] IT Support Ticket No. %Ticket_No%',
                'request_status'=>2,
                'body'=>'Hi, %requestor_name%. Your request with TIcket No. %Ticket_No% has been resolved. To close this ticket, please rate our IT services by answering below satisfactory survey.',
            ],
            [
                'subject'=>'[CLOSED] IT Support Ticket No. %Ticket_No%',
                'request_status'=>6,
                'body'=>'Hi, %requestor_name%. Your request with TIcket No. %Ticket_No% has been closed. You may view ticket trail by clicking the button below.',
            ],
            [
                'subject'=>'[APPROVED] IT Support Ticket No. %Ticket_No%',
                'request_status'=>7,
                'body'=>'Hi, %requestor_name%. Your request with TIcket No. %Ticket_No% has been approved. An IT representative will assist you shortly.',
            ],
            [
                'subject'=>'[DISAPPROVED] IT Support Ticket No. %Ticket_No%',
                'request_status'=>8,
                'body'=>'Hi, %requestor_name%. Your request with TIcket No. %Ticket_No% has been disapproved. ',
            ],
            [
                'subject'=>'[ON HOLD] IT Support Ticket No. %Ticket_No%',
                'request_status'=>1,
                'body'=>'Hi, %requestor_name%. We’ve sorry for the inconvenience as your request with TIcket No. %Ticket_No% has been put on hold by %IT_Staff_Name%. To view information on this request, you may click on the button below.',
            ],
            [
                'subject'=>'[DELETED] IT Support Ticket No. %Ticket_No%',
                'request_status'=>3,
                'body'=>'Hi, %requestor_name%. Your request with TIcket No. %Ticket_No% has been deleted. To create a new ticket, you may click the button below.',
            ],
            [
                'subject'=>'[CANCELLED] IT Support Ticket No. %Ticket_No%',
                'request_status'=>4,
                'body'=>'Hi, %requestor_name%. Your request with TIcket No. %Ticket_No% has been cancelled. To create a new ticket, you may click the button below.',
            ],
            [
                'subject'=>'[OVERDUE] IT Support Ticket No. %Ticket_No%',
                'request_status'=>10,
                'body'=>'Hi, %IT_Staff_Name%. %requestor_name% \'s request with Ticket No. %Ticket_No% is now overdue. To view information on this request, you may click on the button below.',
            ],
            [
                'subject'=>'[ASSIGNED] IT Support Ticket No. %Ticket_No%',
                'request_status'=>13,
                'body'=>'Hi, %requestor_name%. Your request with TIcket No. %Ticket_No% has been assigned to %IT_Staff_Name%. You may view ticket\'s progress online by clicking the button below.',
            ]
        ]);
    }
}
