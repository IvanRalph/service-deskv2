<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Resolution extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
    protected $table = 'resolution_codes';
    public $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = ['code', 'description', 'status'];

    protected $auditInclude = [
            'code',
            'description',
            'status'
        ];
}
