<?php

use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type')->insert([
        	[
        		'title'=>'Incident Report',
        		'description'=>'Test',
        		'created_by'=>1
        	],
        	[
        		'title'=>'Service Request',
        		'description'=>'Test',
        		'created_by'=>1
        	]
        ]);
    }
}
