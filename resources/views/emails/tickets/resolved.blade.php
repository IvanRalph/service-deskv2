@component('mail::message')
# {{ $content['subject'] }}

{{ $content['body'] }}

@component('mail::button', ['url' => config('app.url').'/survey/'. $content['ticket_id'].'/'.$content['survey_token']])
Close Ticket
@endcomponent

You may view ticket trail by clicking the button below.

@component('mail::button', ['url' => config('app.url')])
Click Here
@endcomponent

Thanks,<br>
{{ config('app.name') }}

<hr>
<span style="font-size: 10px;">If you’re having trouble clicking the button above, you may copy and paste the URL below into your web browser: {{ config('app.url') }}
</span>
@endcomponent
