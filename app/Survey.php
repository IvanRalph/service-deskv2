<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Survey extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
    protected $table = 'survey';
    public $primaryKey = 'id';

    protected $fillable = ['request', 'rate', 'staff'];

    public $timestamps = false;

    protected $auditInclude = [
            'request',
            'rate',
            'staff'
        ];
}
