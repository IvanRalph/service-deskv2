<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Ticket extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $table = 'requests';
    public $primaryKey = 'id';

    public $timestamps = false;
    public $fillable = ['ticket_no', 'request_type', 'description', 'priority_level', 'status', 'requested_by', 'assigned_to', 'remarks', 'updated_by', 'resolution_id'];

    protected $auditInclude = [
        'request_type',
        'description',
        'priority_level',
        'status'
    ];

    public function requestType(){
    	return $this->belongsTo('App\RequestType', 'request_type');
    }

    public function priorityLevel(){
    	return $this->belongsTo('App\PriorityLevel', 'priority_level');
    }

    public function statusChild(){
        return $this->belongsTo('App\Status', 'status');
    }

    public function userChild(){
        return $this->belongsTo('App\User', 'requested_by');
    }

    public function assignedTo(){
        return $this->belongsTo('App\User', 'assigned_to');
    }

    public function itStaff(){
        return $this->belongsTo('App\User', 'assigned_to');
    }

    public function survey(){
        return $this->hasOne('App\SurveyToken', 'id');
    }

    public function requestStatusTracker(){
        return $this->hasMany('App\RequestStatus', 'request');
    }
}
