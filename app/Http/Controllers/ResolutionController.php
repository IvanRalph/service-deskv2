<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resolution;

class ResolutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.resolutionCodes');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.createResolution');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'description'=>'required|max:500'
        ]);

        if($validator->fails()){
            $request->flash();
            return redirect(action('ResolutionController@create'))
                    ->withErrors($validator);
        }

        $res = Resolution::orderBy('id', 'DESC')->first();

        if(!$res){
            $res = new \stdClass();
            $res->id = 1;
        }else{
            $res->id += 1;
        }

        $pre = '';
        for($i = 4; $i > strlen($res->id); $i--){
            $pre .= '0';
        }

        Resolution::create([
            'code'=>'ITSD-RES-'.$pre.$res->id,
            'description'=>$request->input('description')
        ]);

        return redirect(action('ResolutionController@index'))->with('resolutionCreated');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.createResolution')->with('updateResolution', Resolution::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->input('activate')){
            $validator = \Validator::make($request->all(), [
                'status'=>'required|in:1,0'
            ]);

            if($validator->fails()){
                \Log::warning('Data has been altered.', ['User'=>\Auth::user()->fullName()]);
                return ['title'=>'Error', 'msg'=>'There was an error with your request.', 'type'=>'error'];
            }

            Resolution::find($id)->update([
                'status'=>$request->input('status')
            ]);

            return ['title'=>'Success', 'msg'=>'Resolution Code updated successfully.', 'type'=>'success'];
        }

        $validator = \Validator::make($request->all(), [
            'description'=>'required|max:500'
        ]);

        if($validator->fails()){
            $request->flash();
            return redirect(action('ResolutionController@edit', $id))
                    ->withErrors($validator);
        }

        Resolution::find($id)->update([
            'description'=>$request->input('description')
        ]);

        return redirect(action('ResolutionController@index'))->with('resolutionUpdated', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
