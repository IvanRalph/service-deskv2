<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->insert([
        	[
        		'status'=>'On Hold'
        	],
        	[
        		'status'=>'Resolved'
        	],
        	[
        		'status'=>'Deleted'
        	],
        	[
        		'status'=>'Cancelled'
        	],
        	[
        		'status'=>'Open'
        	],
        	[
        		'status'=>'Closed'
        	],
        	[
        		'status'=>'Approved'
        	],
        	[
        		'status'=>'Disapproved'
        	],
        	[
        		'status'=>'Reopened'
        	],
        	[
        		'status'=>'Over due'
        	],
            [
                'status'=>'For Approval'
            ],
            [
                'status'=>'For Approval'
            ],
            [
                'status'=>'Assigned'
            ]
        ]);
    }
}
