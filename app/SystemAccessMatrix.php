<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class SystemAccessMatrix extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;
    protected $table = 'access_matrix';
    public $primaryKey = 'id';

    public $timestamps = false;

    protected $auditInclude = [
            'requestor',
            'approver',
            'it_staff',
            'it_head'
        ];
}
