<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyToken extends Model
{
    protected $table = 'survey_token';
    public $primaryKey = 'id';

    protected $fillable = ['request_id', 'token'];

    public $timestamps = false;

    public function ticket(){
    	return $this->belongsTo('App\Ticket', 'id');
    }
}
