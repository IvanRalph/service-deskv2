@extends('adminlte::page')

@section('title', 'Resolution Codes | IT Service Desk')

@section('content_header')

@stop

@section('content')
<div class='notifications top-right'></div>
	<div class="row">
		<div class="col-sm-2">
			<a href="{{ action('ResolutionController@index') }}" style="color: #333; font-size: 35px!important;"><i class="fa fa-arrow-left"></i></a>
		</div>
	</div>
	<div class="row">
		<form action="{{ isset($updateResolution) ? action('ResolutionController@update', $updateResolution->id) : action('ResolutionController@store') }}" method="POST">
			@csrf
			@isset ($updateResolution)
			@method('PATCH')
			@endisset
			<div class="col-md-6 col-md-offset-3">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title"><i class="fa fa-support"></i> Create Resolution</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="form-group">
							<label for="code">Resolution Code</label>
							<input type="text" value="{{ isset($updateResolution) ? $updateResolution->code : 'ITSD-RES-000X' }}" class="form-control" disabled name="">
						</div>

						<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
							<label for="description">Description</label>
							<textarea class="form-control" name="description" id="description">{{ isset($updateResolution) ? $updateResolution->description : '' }}</textarea>
							@if($errors->has('description'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('description') }}</span>
							@endif
						</div>
					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<div class="text-center">
							<input type="submit" id="_submit" hidden>
							<button name="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Loading..">Submit</button>
							<button name="cancel" class="btn btn-default">Discard</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
@stop

@section('js')
	<script type="text/javascript">
		$('[name="submit"]').on('click', function(e){
			e.preventDefault();
			swal({
				type: 'question',
				text: 'Are you sure you want to {{ isset($updateResolution) ? 'update this' : 'create a' }} resolution?',
				showConfirmButton: true,
				showCancelButton: true,
				confirmButtonText: 'Yes',
				cancelButtonText: 'No',
				showLoaderOnConfirm: true
			}).then(function(){
				$('[name="submit"]').button('loading');
				$('#_submit').click();
			});
		})
	</script>
@stop