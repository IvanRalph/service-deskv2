<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriorityLevel extends Model
{
    protected $table = 'priority_levels';
    public $primaryKey = 'id';

    public function Ticket(){
    	return $this->hasMany('App\Ticket', 'id');
    }
}
