<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SystemAccessMatrix;

class AccessMatrixController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.accessMatrix');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $count = SystemAccessMatrix::count();
        $acl = SystemAccessMatrix::get();
        for ($i=1; $i <= $count; $i++) { 
            SystemAccessMatrix::where('id', $acl[$i-1]->id)->update([
                'requestor'=>$request->input('req_'. $i) != null ? 1 : 0,
                'approver'=>$request->input('app_'. $i) != null ? 1 : 0,
                'it_staff'=>$request->input('it_'. $i) != null ? 1 : 0,
                'it_head'=>$request->input('head_'. $i) != null ? 1 : 0
            ]);
        }

        return redirect('/setup/access-matrix');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
